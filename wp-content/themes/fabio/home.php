<?php
/**
 * Description: Default Home template to display loop of blog posts
 *
 * @package WordPress
 * @subpackage fabio
 */ 

get_header(); ?>

<?php
//Get the ID of the page used for Blog posts
$page_id = ( 'page' == get_option( 'show_on_front' ) ? get_option( 'page_for_posts' ) : get_the_ID() );

$css_class         = ( 'page' == get_option( 'show_on_front' ) ?'fabio_blog' : 'fabio_page' );

?>

<section id="content" class="<?php echo esc_attr($css_class); ?>">
    <div class="container">
        <div class="row">
          <main>
            <div class="Current_Home">
              <div class="Bloc Deco1">
                <img src="/wp-content/uploads/2019/09/FW19-035-DL.jpg" onclick="goto('/boutique/fw19/belt-in-crepe-1-5cm/')"><div class="tiers">
                  <h1>Current Collection</h1>
                  <a href="/product-category/fw19/">discover</a>
                </div><img src="/wp-content/uploads/2019/09/FW19-020-JS.jpg" onclick="goto('/boutique/fw19/belt-in-printed-crepe-and-leather/')">
              </div>
              <div class="Bloc Deco2">
                <img style=" width: 100%; max-width: 400px;" src="/wp-content/uploads/2019/09/Honayda-low-res.jpg"><div class="half">
                  <h1>Brand</h1>
                  <a href="/brand">learn more</a>
                </div>
              </div>
            </div>
          </main>
          <div class="Bande">FW19/20</div>
          <div class="col">
            <div class="row">
              <div style="display:flex; margin-left:4em; justify-content: space-between">
                <div class="bottom-text-block">
                  <h3 style="margin-bottom:2em"><strong>FW19/ One Love</strong></h3>
                  <p class="p1" style="font-size:14px!important;">On one of those warm winter days, while conversing with a
                    friend who was about to get married, we started reminiscing about those ancient times when women used to
                    dream of their ‘prince charming’ and hope to be the lucky ones who got married. Marriage rhymed with
                    life; and women dreamt of feeling alive.</p>
                  <p class="p1" style="font-size:14px!important;">Brides in the old traditions used to receive magically
                    precious gifts inspired by the tales of a thousand and one night. One exquisite perfume bottle was the
                    ultimately awaited gift. It had those gold and silver crown-like ornaments, with the bride and groom’s
                    names hand-engraved and holding a unique aroma. Women used to wait for that day to live their
                    fairy-tale, but nowadays, women have achieved so much that they can buy their own magic.</p>
                  <p class="p1" style="font-size:14px!important;">The ‘One Love’ collection goes beyond this perfume bottle
                    itself while digging into what’s inside it. A woman’s scent diffuses aspects of her personality; it
                    wears her in the same way she wears it. Each woman is unique in every single fragment of her being, and
                    the Fall/Winter 19 collection translates this sense of exclusivity as each piece stands unique in its
                    own design.</p>
                  <p class="p1" style="font-size:14px!important;">It is a tribute to all those successful women, cultured
                    and hard-working, who have earned their independence and learned to know their worth in marriage.</p>
                  <p class="p1" style="font-size:14px!important;">To all the women who have inspired my collections so far,
                    ‘One Love’ comes to brace their legendary beauty through the color palette of a cloudy pink and starry
                    grey on soft tulle and delicate chiffon redefining 21<span class="s2">st </span>century chic. The noble
                    black and flawless white come to compliment the female figure’s exceptional strength and the timeless
                    light she beams, while geometrical motifs make their return as part of Honayda’s brand codes.</p>
                  <p class="p1" style="font-size:14px!important;">The elegant perfume bottle has particularly inspired this
                    season’s embroidery with the edgy metallics, nylon, and sprinkled beads that add this magical sparkle to
                    the collection’s contemporary lines.</p>
                  <p class="p1" style="font-size:14px!important;">Continuing to narrate the stories of confident, astute and
                    energetic women, these figures will forever be at the center of my creations for I believe that a woman
                    can unlock the toughest paths.</p>
                </div>
                <div style="-webkit-box-flex: 0; flex: 0 0 50%; max-width:50%; height:auto; margin: 0">
                  <img style=" width: 100%; max-width: 480px; height: auto;margin-left: auto; margin-right: auto; display: block;margin-top:3em " src="/wp-content/uploads/2019/02/HONAYDA5828-fp.jpg">
                </div>
              </div>
            </div>
          </div>

        </div><!-- .row -->
    </div><!-- .container -->    
</section>
<?php get_footer(); ?>