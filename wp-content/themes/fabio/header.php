<?php
/**
 * Default Page Header
 *
 * @package WordPress
 * @subpackage fabio
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="st-container" class="st-container">

	<!-- content push wrapper -->
	<div class="st-pusher">
		<?php get_template_part('parts/woocommerce/part','sidebar');?>
		<div class="st-content"><!-- this is the wrapper for the content -->
			<div class="st-content-inner"><!-- extra div for emulating position:fixed of the menu -->
			
<?php if( _get_field('gg_site_preloader', 'option', true) ) : ?>
<div class="spinner_wrapper">
	<div class="spinner">
		<div class="spinner__item1"></div>
		<div class="spinner__item2"></div>
		<div class="spinner__item3"></div>
		<div class="spinner__item4"></div>
	</div>
</div>
<?php endif; ?>

<header class="site-header default  ">

<?php get_template_part( 'lib/headers/part','default-menu' ); ?>

</header>
<div class="content-wrapper">
<?php fabio_page_header(); ?>
<!-- End Header. Begin Template Content -->