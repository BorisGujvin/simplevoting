<?php
if ( ! function_exists( 'fabio_cart_link_fragment' ) ) {
    /**
     * Cart Fragments
     * Ensure cart contents update when products are added to the cart via AJAX
     *
     * @param  array $fragments Fragments to refresh via AJAX.
     * @return array            Fragments to refresh via AJAX
     */
    function fabio_cart_link_fragment( $fragments ) {
        global $woocommerce;
        ob_start();
        fabio_header_cart_link();
        $fragments['a.cart-shop-top'] = ob_get_clean();
        fabio_cart_count();
        $fragments['span.modal-cart-count'] = ob_get_clean();
        echo '<div class="fabio-widget_shopping_cart_content">';
        woocommerce_mini_cart();
        echo '</div>';
        $fragments['div.fabio-widget_shopping_cart_content'] = ob_get_clean();

        return $fragments;
    }
}
add_filter('woocommerce_add_to_cart_fragments', 'fabio_cart_link_fragment');

if ( ! function_exists( 'fabio_header_cart_link' ) ) {
    /**
     * Cart Link
     * Displayed a link to the cart including the number of items present and the cart total
     *
     * @return void
     * @since  1.0.0
     */
    function fabio_header_cart_link() {
        ?>
            <a class="cart-shop-top" href="#simpleModal" id="st-trigger-effects">
                <i class="fa fa-cart-plus"></i>
                <span class="gg-products-count"><?php echo wp_kses_data( WC()->cart->get_cart_contents_count() );?></span>
            </a>
        <?php
    }
}

if ( ! function_exists( 'fabio_cart_count' ) ) {
    /**
     * Cart Link
     * Displayed a link to the cart including the number of items present and the cart total
     *
     * @return void
     * @since  1.0.0
     */
    function fabio_cart_count() {
            $items = WC()->cart->get_cart();
        ?>
           
            <span class="modal-cart-count">
                <?php
                    if ( $items ) {
                        echo wp_kses_data( sprintf( _n( '%d item in cart', '%d items in cart', WC()->cart->get_cart_contents_count(), 'fabio' ), WC()->cart->get_cart_contents_count() ) );
                    }
                ?>
            </span>
        <?php
    }
}


/** New functions */

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 30 );

remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );


function fabio_woocommerce_before_shop_loop_item() {
	
	echo '<div class="product-image">';
    echo '<a class="product-image-overlay-link" href="'.esc_url(get_the_permalink()).'"></a>';
    
    if ( class_exists('YITH_WCWL') ) {
        echo '<div class="fabio-wishlist-icon">';
        echo do_shortcode('[yith_wcwl_add_to_wishlist]');
        echo '</div>';
    }

	echo '<span class="overlay">';
	echo '<span class="wrapper">';

    	echo woocommerce_template_loop_add_to_cart();

	echo '</span>';
	echo '</span>';

	echo get_the_post_thumbnail( get_the_id(),'full' );

	echo '</div>';
}
add_action('woocommerce_before_shop_loop_item','fabio_woocommerce_before_shop_loop_item');

function fabio_woocommerce_before_shop_loop_item_title(){
	echo '<div class="product-body">';
	
	global $product; 
	if ( _get_field('gg_store_products_price','option', true) === true ) {
		echo '<span class="price">'.$product->get_price_html() .'</span>';
	}
	echo '<a href="'.get_the_permalink().'">';
	
}
add_action('woocommerce_before_shop_loop_item_title','fabio_woocommerce_before_shop_loop_item_title');

function fabio_woocommerce_after_shop_loop_item_title(){
	echo '</a></div>';
}
add_action('woocommerce_after_shop_loop_item_title','fabio_woocommerce_after_shop_loop_item_title');



if(isset($_GET['woo_per_page'])){
	$per_page = (int) $_GET['woo_per_page'];
	add_filter( 'loop_shop_per_page', create_function( '$cols', 'return '.$per_page.';' ), 20 );
}

/** 
 * Remove tab headings
 */
add_filter('woocommerce_product_description_heading', 'fabio_clear_tab_headings');
add_filter('woocommerce_product_additional_information_heading', 'fabio_clear_tab_headings');
function fabio_clear_tab_headings() {
    return '';
}


/*
 * Modal cart get items
 */

function fabio_get_cart_items() {
    fabio_cart_count();
    echo '<div class="fabio-widget_shopping_cart_content">';
	woocommerce_mini_cart();
    echo '</div>';

}
/* Show the cart on Cart and Checkout pages */
add_filter( 'woocommerce_widget_cart_is_hidden', 'always_show_cart', 40, 0 );
function always_show_cart() {
    return false;
}
/*
 * Modal login user callback
 */
function fabio_login_user_callback(){

	check_ajax_referer( 'woocommerce-login','woocommerce-login-nonce');

    $creds = array(
        'user_login'    => sanitize_text_field($_POST['username']),
        'user_password' => sanitize_text_field($_POST['password']),
        'remember'      => (isset($_POST['rememberme']))?true: false
    );
 
    $user = wp_signon( $creds, false );
	
    if ( is_wp_error( $user ) ) {
        echo $user->get_error_message();
    } else {
		echo '1';
	}
	wp_die();
}
add_action( 'wp_ajax_fabio_login_user', 'fabio_login_user_callback' );
add_action( 'wp_ajax_nopriv_fabio_login_user', 'fabio_login_user_callback' );

/*
 * Modal register user callback
 */
function fabio_register_user_callback(){
	
    check_ajax_referer( 'woocommerce-register','woocommerce-register-nonce');

	$user_email = sanitize_text_field($_POST['email']);
	$username = sanitize_text_field($_POST['username']);
	$password = sanitize_text_field($_POST['password']);
	
    if(empty($username)){
		if(!empty($user_email)){
			$username = explode('@',$user_email);
			$username = $username[0];
		}
	}

	$user_id = username_exists( $username );
	if ( !$user_id and email_exists($user_email) == false ) {

		$user_id = wp_create_user( $username, $password, $user_email );
		if(!is_numeric($user_id)){
			wp_die($user_id->get_error_message());
		}
		$creds = array(
			'user_login'    => $username,
			'user_password' => $password,
			'remember'      => false
		);
	 
		$user = wp_signon( $creds, false );
		wp_die('1');
	} else {
		wp_die( esc_html__('User already exists!', 'fabio') );
	}
		
	wp_die();
}
add_action( 'wp_ajax_fabio_register_user', 'fabio_register_user_callback' );
add_action( 'wp_ajax_nopriv_fabio_register_user', 'fabio_register_user_callback' );

/*
 * Include category custom fields
 */
include( get_template_directory().'/lib/woocommerce-cat_custom_fields.php' );

/*
 * Set the woo shop pagination from get.
 */
if(isset($_GET['woo_per_page'])){
	$per_page = (int)$_GET['woo_per_page'];
	setcookie("fabio_shop_per_page", $per_page, time()+3600*24*31); // keep for 1 month
}	

/*
 * Get YITH wishlist page
 */
if (!function_exists('fabio_get_wishlist_page')) {
    function fabio_get_wishlist_page(){
    	$id = get_option('yith_wcwl_wishlist_page_id');
    	return get_permalink($id);
    }
}

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );

add_action( 'fabio_woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
add_action( 'fabio_woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );

remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
add_action( 'woocommerce_checkout_after_customer_details', 'woocommerce_checkout_payment', 20 );