<?php
/**
 * WooCommerce Sold Out Products
 *
*/
add_action( 'woocommerce_before_single_product_summary', 'fabio_sold_out_products_flash', 9 );
add_action( 'woocommerce_before_shop_loop_item_title', 'fabio_sold_out_products_flash', 9 );

/**
 * add sold out text to the product image
 */
function fabio_sold_out_products_flash() {
	global $post,$product;

	if ( ! $product->is_in_stock() ) {
		echo apply_filters( 'woocommerce_sale_flash', '<span class="soldout">'.esc_html__( 'Sold Out!', 'fabio' ).'</span>', $post, $product );
	}
}

?>