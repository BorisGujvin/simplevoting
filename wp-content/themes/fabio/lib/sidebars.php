<?php
/**
 * Define theme's widget areas.
 *
 */
function fabio_widgets_init() {

    //Construct the default sidebars array
    $sidebars_array = array(
        esc_html__('Page sidebar', 'fabio')          => 'sidebar-page',
        esc_html__('Posts sidebar', 'fabio')         => 'sidebar-posts',
        esc_html__('Search sidebar', 'fabio')        => 'sidebar-search',
        esc_html__('Shop sidebar', 'fabio')          => 'sidebar-shop',
        esc_html__('Product sidebar', 'fabio')       => 'sidebar-product',
        esc_html__('Footer first sidebar', 'fabio')  => 'sidebar-footer-first',
        esc_html__('Footer second sidebar', 'fabio') => 'sidebar-footer-second',
        esc_html__('Footer third sidebar', 'fabio')  => 'sidebar-footer-third',
        esc_html__('Footer fourth sidebar', 'fabio') => 'sidebar-footer-fourth'
    );


    foreach ($sidebars_array as $sidebar_name => $sidebar_id) {
        register_sidebar(
            array(
                'name'          => $sidebar_name,
                'id'            => $sidebar_id,
                'description'   => sprintf(esc_html__('Add widgets here to appear in your %1$s','fabio'), $sidebar_name),
                'before_widget' => '<div id="%1$s" class="gg-widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4 class="widget-title">',
                'after_title'   => '</h4>'
            )
        );
    }
    
    //Dynamic sidebars
    if ( class_exists( 'acf' ) ) { //Check to see if ACF is installed
        if (_get_field('gg_sidebars','option')) {
            while (_has_sub_field('gg_sidebars','option')) { //Loop through sidebar fields to generate custom sidebars

                $s_name = _get_sub_field('gg_sidebar_name','option');
                $s_id   = str_replace(" ", "-", $s_name); // Replaces spaces in Sidebar Name to dash
                $s_id   = strtolower($s_id); // Transforms edited Sidebar Name to lowercase
                
                register_sidebar( array(
                    'name'          => $s_name,
                    'id'            => $s_id,
                    'before_widget' => '<div id="%1$s" class="gg-widget %2$s">',
                    'after_widget'  => '</div>',
                    'before_title'  => '<h4 class="widget-title">',
                    'after_title'   => '</h4>',
                ) );
            }
        }
    }
}
add_action('init', 'fabio_widgets_init');
?>