<?php
/**
 * Woocommerce Custom category fields
 **/
if ( ! class_exists( 'Fabio_Product_Custom_Fields' ) ) {

class Fabio_Product_Custom_Fields {
public function __construct(){}
 
 /*
  * Initialize the class and start calling our hooks and filters
  * @since 1.0.0
 */
 public function init() {
  $taxonomy  = 'product_cat';
   add_action( $taxonomy.'_add_form_fields', array ( $this, 'add_category_image' ), 10, 2 );
   add_action( 'created_'.$taxonomy, array ( $this, 'save_category_image' ), 10, 2 );
   add_action( $taxonomy.'_edit_form_fields', array ( $this, 'update_category_image' ), 10, 2 );
   add_action( 'edited_'.$taxonomy, array ( $this, 'updated_category_image' ), 10, 2 );
 }
 
 /*
  * Add a form field in the new category page
  * @since 1.0.0
 */
 public function add_category_image ( $taxonomy ) { ?>
   <div class="form-field term-group clear">
     <label for="box-image"><?php esc_html_e('Box Image', 'fabio'); ?></label>
     <input type="text" id="box-image" name="box-image" class="custom_media_url" value="">
     <div id="box-image-wrapper"></div>
     <p>
       <input type="button" class="button button-secondary ct_tax_media_button1" data-type="add" data-wrapper="#box-image-wrapper" data-target="#box-image" id="ct_tax_media_button1"  name="ct_tax_media_button1" value="<?php esc_html_e( 'Add Image', 'fabio' ); ?>" />
       <input type="button" class="button button-secondary ct_tax_media_remove1" data-type="add" data-wrapper="#box-image-wrapper" data-target="#box-image" id="ct_tax_media_remove1" name="ct_tax_media_remove1" value="<?php esc_html_e( 'Remove Image', 'fabio' ); ?>" />
    </p>
   </div>

 <?php
 }
 
 /*
  * Save the form field
  * @since 1.0.0
 */
 public function save_category_image ( $term_id, $tt_id ) {
   if( isset( $_POST['box-image'] ) && '' !== $_POST['box-image'] ){
     $image = $_POST['box-image'];
     add_term_meta( $term_id, 'box-image', $image, true );
   }

 }
 
 /*
  * Edit the form field
  * @since 1.0.0
 */
 public function update_category_image ( $term, $taxonomy ) { ?>
   <tr class="form-field term-group-wrap">
     <th scope="row">
       <label for="box-image-id"><?php esc_html_e( 'Box Image', 'fabio' ); ?></label>
     </th>
     <td>
       <?php $image_id = get_term_meta ( $term->term_id, 'box-image', true ); ?>
       <input type="hidden" id="box-image" name="box-image" value="<?php echo esc_html($image_id); ?>">
       <div id="box-image-wrapper">
         <?php if ( $image_id ) { ?>
           <?php echo wp_get_attachment_image ( $image_id, 'thumbnail', true, array('width'=>100, 'height'=> 100) ); ?>
         <?php } ?>
       </div>
       <p>
         <input type="button" class="button button-secondary ct_tax_media_button1" data-type="update"  data-wrapper="#box-image-wrapper" data-target="#box-image"  id="ct_tax_media_button1" name="ct_tax_media_button1" value="<?php esc_html_e( 'Add Image', 'fabio' ); ?>" />
         <input type="button" class="button button-secondary ct_tax_media_remove1" data-type="update" data-wrapper="#box-image-wrapper" data-target="#box-image" id="ct_tax_media_remove1" name="ct_tax_media_remove1" value="<?php esc_html_e( 'Remove Image', 'fabio' ); ?>" />
       </p>
     </td>
   </tr>

 <?php
 }

/*
 * Update the form field value
 * @since 1.0.0
 */
 public function updated_category_image ( $term_id, $tt_id ) {
	 
   if( isset( $_POST['box-image'] ) && '' !== $_POST['box-image'] ){
     $image = $_POST['box-image'];
     update_term_meta ( $term_id, 'box-image', $image );
   } else {
     update_term_meta ( $term_id, 'box-image', '' );
   }
  
 }

}
 
$CT_TAX_META = new Fabio_Product_Custom_Fields();
$CT_TAX_META -> init();
 
}