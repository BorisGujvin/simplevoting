<nav class="navbar navbar-default">
    <div class="container top-bar">
        <ul class="top-header">

          <li class="logo">
            <?php get_template_part( 'parts/part-header-logo');?>
          </li>

        	<li class="menus">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar-collapse" aria-expanded="true">
                    <span class="sr-only"><?php esc_html_e('Toggle navigation','fabio'); ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-collapse collapse" id="main-navbar-collapse">
                    <?php
                    if ( _get_field('gg_header_mainmenu', 'option', true) ) {
                        get_template_part( 'lib/headers/part','top-menu' );
                    }
                    ?>
                    <?php
                    if ( _get_field('gg_header_secondarymenu', 'option', true) ) {
                        get_template_part( 'lib/headers/part','bottom-menu' );
                    }
                    ?>
                </div>
        	</li>

            <li class="shop">
            	<ul class="flex-right">

        			<?php if ( class_exists( 'YITH_WCWL' ) && _get_field('gg_header_wishlist', 'option', true) ) : ?>
                    <li class="wishlist">
                        <a href="<?php echo fabio_get_wishlist_page();?>">
                            <i class="fa fa-heart visible-xs"></i>
                            <span class="hidden-xs"><?php esc_html_e('Wishlist','fabio');?></span>
                        </a>
                    </li>
                    <?php endif; ?>
            		
                    <?php if( has_action('wpml_add_language_selector') && _get_field('gg_header_languages', 'option', true) ) : ?>
            		<li>
        				<div class="top-bar-flags">
        					<?php echo fabio_wpml_language_sel(); ?>
        				</div>    			
            		</li>
            		<?php elseif( defined('POLYLANG_VERSION') && _get_field('gg_header_languages', 'option', true) ) : ?>
            		<li>
            			<div class="top-bar-flags">
        					<ul>
        						<?php pll_the_languages( array( 'show_flags' => 1,'show_names' => 1, 'display_names_as' => 'slug' ) ); ?>
        					</ul>
        				</div>
            		</li>
            		<?php endif; ?>

                    <?php if( _get_field('gg_header_search', 'option', true) ) : ?>
                    <li class="top-search">
                        <a href="#search" class="top-search-trigger">
                            <i class="fa fa-search"></i>
                        </a>
                    </li>
                    <?php endif; ?>

            		<?php if( fabio_is_wc_activated() && _get_field('gg_header_minicart', 'option', true) ) : ?>
            		<li class="top-cart">
        		    	<?php fabio_header_cart_link(); ?>
            		</li>
            		<?php endif; ?>
            	</ul>
            </li>
        </ul>
    </div>
</nav>
