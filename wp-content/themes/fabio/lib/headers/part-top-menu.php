	<!-- Begin Main Navigation -->
	<?php
	wp_nav_menu(
		array(
			'theme_location'    => 'main-menu',
			'container'         => '',
			'container_class'   => '',
			'menu_class'        => 'nav navbar-nav',
			'fallback_cb'       => 'fabio_navwalker::fallback',
			'menu_id'           => 'main-menu',
			'walker'            => new fabio_navwalker()
		)
	); ?>
	<!-- End Main Navigation -->

