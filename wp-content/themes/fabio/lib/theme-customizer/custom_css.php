<?php
/*Custom CSS*/

//Font
$gg_headings_font = _get_field('gg_headings_font', 'option', array('font' => 'Montserrat' ));
$gg_body_font = _get_field('gg_body_font', 'option',array('font' => 'Open Sans' ));

//Text color
$gg_text_body_color = _get_field('gg_text_body_color', 'option','#9c9c9c');
$gg_headings_color = _get_field('gg_headings_color', 'option','#000000');
$gg_link_color = _get_field('gg_link_color', 'option','#121416');

//Primary color
$gg_primary_color = _get_field('gg_primary_color', 'option','#000000');
//Secondary color
$gg_secondary_color = _get_field('gg_secondary_color', 'option','#f2f6f7');
?>

<?php if ( $gg_headings_font['font'] != 'Montserrat' ) : ?>
	h1, h2, h3, h4, h5, h6,
    .table>thead>tr>th,
    body .wrap-forms label,
    label,
    .wc-proceed-to-checkout .checkout-button,
    .woocommerce .product .single_add_to_cart_button.button,
    input[type=submit],
    .single_add_to_cart_button,
    a.btn,
    a.btn-default:visited,
    a.btn-default:focus,
    a.btn-default:active,
    input.btn,
    input.btn-default:visited,
    input.btn-default:focus,
    input.btn-default:active,
    .top-bar .logo-wrapper h2,
    .top-bar .logo-wrapper div,
    .top-bar .cart-shop-top span.gg-products-count,
    .gg-searchform-wrapper input,
    #main-menu li a,
    .page-meta-wrapper h1,
    .fabio-post1-header h2,
    .posts2 .gg-module-title h2,
    .products-carousel-title h2,
    .fabio-new-items-wrapper .fabio-categories  a,
    .fabio-new-items-wrapper .fabio-collection-categories a,
    .box-1 .box-1-left-column .box-item h4,
    .box-1 .box-1-left-column h2,
    .instagram .instagram-left h2,
    .instagram .instagram-right .followers,
    .footer-widgets .gg-widget.widget_nav_menu ul li a,
    .footer-widgets .gg-widget.widget_pages ul li a,
    .footer-widgets .gg-widget.widget_categories ul li a,
    .footer-widgets .gg-widget.widget_archive ul li a,
    .footer-widgets-holder h4,
    .contact-us-module-wrapper,
    .social-icons-shortcode ul li a,
    body .fw-package .fw-heading-row span, 
    body .fw-package .fw-pricing-row span,
    body .fw-tabs-container .fw-tabs ul li a,
    body .fw-iconbox .fw-iconbox-title h3,
    body .fw-testimonials-2 .fw-testimonials-author .author-name,
    .gg_posts_grid .fabio-datetime,
    .entry-content a.more-link,
    .unyson-post-excerpt p a span,
    .gg_posts_grid .post-meta,
    aside.sidebar-nav h4,
    .gg-widget .tagcloud a,
    .fabio-posts-widget ul li .cnt .title a,
    .posts1 .fabio-post1-categories a,
    .posts2 .fabio-post1-categories a,
    .post1-item .post1-image .overlay .title,
    .fabio_countify .countify_before,
    .fabio_countify .countify_after,
    .fabio_countify .countify_number,
    .fabio_countify .countify_label,
    .single-post .posted-in-tags,
    .single-post .posted-in-categories,
    .single-post .post-date,
    blockquote:after,
    .comments-area .media-list .media .media-body h4 a,
    .fabio-gallery-single-header .toRight a,
    div.pagination ul.pagination li a,
    .widget_price_filter .price_slider_amount .button,
    .product_widget .price_slider_wrapper .price_slider_amount button,
    .more-link,
    .open > .dropdown-toggle.btn-primary:hover,
    .open > .dropdown-toggle.btn-primary:focus,
    .open > .dropdown-toggle.btn-primary.focus,
    mark,
    .mark,
    .widget_price_filter .ui-slider::before,
    .u-columns .forgot-remember .rememberme,
    body .fw-call-to-action .fw-action-content h2,
    .collection-carousel-pagination .view-button,
    .collection-carousel-wrapper .collection-carousel-pagInfo,
    .gg-breadcrumbs .delimiter,
    .nav_crumb .delimiter,
    .sidebar-nav ul li a,
    .sidebar-nav ol li a,
    #secondary-menu .mega-menu .mega-menu-col > a,
    .fabio-faq .faq-title,
    .fabio-modal .fabio-modal-wrapper .fabio-modal-content .close a,
    .woocommerce-form-login .form-row-button .remember-me,
    span.onsale,
    span.soldout,
    .woocommerce select.fabio_woo_result_count,
    .woocommerce form.woocommerce-ordering select,
    .woocommerce .products-per-page select,
    .product-body > a > h3,
    .product-body > a > h2,
    .product-image .overlay .wrapper a,
    .product .tooltip-inner,
    .woocommerce .product .summary h1.product_title,
    .woocommerce .product .summary .variations td.label,
    .woocommerce-tabs .tabs li a,
    body.woocommerce .wishlist_table td.product-stock-status,
    body.woocommerce .wishlist_table td.product-add-to-cart a,
    .woocommerce .shop_table.cart .product-name .product-remove a.remove,
    body.woocommerce-checkout h2,
    body.woocommerce-checkout h3,
    .woocommerce form.checkout #customer_details h3,
    .woocommerce form.checkout #order_review header.title h3 ,
    #ship-to-different-address label,
    .woocommerce form #shipping_method li label,
    .woocommerce form #shipping_method li label .woocommerce-Price-amount,
    .woocommerce form.checkout ul.payment_methods li label,
    .woocommerce .shop_attributes th,
    .woocommerce .shop_table.cart th,
    .woocommerce-MyAccount-navigation ul li a,
    .woocommerce .cart-actions .coupon label,
    .woocommerce .cart-collaterals h2,
    .single-product .product .summary .price,
    .gg-header-minicart h2,
    #simpleModal .close-window {
		font-family: <?php echo esc_html($gg_headings_font['font']); ?>;
		letter-spacing: 0;
		font-weight: normal;
	}

	body .wrap-forms input:-moz-placeholder,
    body .wrap-forms textarea:-moz-placeholder {
        font-family: <?php echo esc_html($gg_headings_font['font']); ?>;
        letter-spacing: 0;
        font-weight: normal;
    }

    body .wrap-forms input::-moz-placeholder,
    body .wrap-forms textarea::-moz-placeholder {
        font-family: <?php echo esc_html($gg_headings_font['font']); ?>;
        letter-spacing: 0;
        font-weight: normal;
    }

    body .wrap-forms input:-ms-input-placeholder,
    body .wrap-forms textarea:-ms-input-placeholder {
        font-family: <?php echo esc_html($gg_headings_font['font']); ?>;
        letter-spacing: 0;
        font-weight: normal;
    }

    body .wrap-forms input::-webkit-input-placeholder,
    body .wrap-forms textarea::-webkit-input-placeholder {
        font-family: <?php echo esc_html($gg_headings_font['font']); ?>;
        letter-spacing: 0;
        font-weight: normal;
    }


<?php endif; ?>

<?php if ( $gg_body_font['font'] != 'Open Sans' ) : ?>
	
	body,
    h6,.h6,
    #main-menu .dropdown-menu li a,
    .navbar-secondary #secondary-menu .dropdown-menu li a,
    .navbar-nav .dropdown-menu li a,
    .fabio-post1-header h6,
    .posts2 .gg-module-title h6,
    .products-carousel-title h6,
    #secondary-menu .mega-menu .mega-menu-col ul.sub-menu li a,
    .woocommerce-form-login .form-row .fabio-woo-password-lost .woocommerce-LostPassword a,
    #reviews #comments .comment .description p,
    .wc_payment_methods .about_paypal,
    .cart_item .woocommerce-Price-amount,
    .woocommerce .cart-collaterals .cart_totals table td,
    .woocommerce .cart-collaterals .cart_totals table th {
		font-family: <?php echo esc_html($gg_body_font['font']); ?>;
		font-style: normal;
	}

<?php endif; ?>


<?php if ($gg_text_body_color != '#9c9c9c') { ?>
	body,
    h6,.h6,
	caption,
    .top-bar .top-header > li.shop .wishlist,
    .top-bar .top-header > li.shop .wishlist a,
    #main-menu li a:hover,
    .navbar-secondary #secondary-menu li a,
    .fabio-post1-header h6,
    .posts2 .gg-module-title h6,
    .products-carousel-title h6,
    .products-carousel .blog-date a,
    .fabio-new-items-wrapper .fabio-categories  a,
    .fabio-new-items-wrapper .fabio-collection-categories a,
    body .fw-team .fw-team-name span,
    .footer-copyright,
    .footer-copyright a,
    .fabio_mailchimp .email p,
    .fabio_mailchimp.light,
    .fabio_mailchimp.light h2,
    .fabio_mailchimp.light .email p,
    body .fw-iconbox .fw-iconbox-text,
    body .fabio-icon-box .fw-iconbox-aside,
    .unyson-post-excerpt p a,
    .gg_posts_grid .post-meta,
    .gg_posts_grid .entry-content,
    .gg_posts_grid .entry-summary,
    .posts1 .fabio-post1-categories a,
    .posts2 .fabio-post1-categories a,
    .posts2 .header .categories,
    .post1-item .post1-image .overlay .categories,
    .comments-area .media-list .media .media-body p.meta a:not(.comment-reply-link),
    .fabio-gallery-single-header .toRight a,
    .fabio-gallery-info .gallery-description,
    #fabio_filter_widget .widget_product_categories ul.product-categories li a,
    .white-modal-item .widget_layered_nav ul li a,
    .white-modal-item .widget_product_tag_cloud .tagcloud a,
    div.pagination ul.pagination li a,
    .u-columns .forgot-remember .lost-password,
    .gg-breadcrumbs a,
    .nav_crumb a,
    .nav_crumb,
    .gg-breadcrumbs,
    #secondary-menu .mega-menu .mega-menu-col ul.sub-menu li a,
    .woocommerce-form-login .form-row .fabio-woo-password-lost .woocommerce-LostPassword a,
    #reviews #comments .comment .description p,
    #reviews #comments .comment p.meta time,
    .woocommerce-tabs .tabs li a,
    .woocommerce-tabs .panel,
    body .wishlist_table tr td.product-stock-status span.wishlist-in-stock,
    body.woocommerce #content table.wishlist_table.cart a.remove,
    .woocommerce .shop_table.cart .product-name h3.product-price,
    .woocommerce .shop_table.cart .product-name .product-remove a.remove,
    .woocommerce .cart-collaterals .cart_totals table tr th,
    body.woocommerce-checkout .woocommerce form.checkout #order_review .shop_table th,
    .woocommerce form.checkout #order_review .shop_table dl.variation dt,
    .woocommerce .woocommerce-checkout-review-order-table .product-name,
    .woocommerce .woocommerce-MyAccount-content > p:not(.woocommerce-FormRow),
    .woocommerce .woocommerce-MyAccount-content > p:not(.form-row),
    .woocommerce-MyAccount-navigation ul li a,
    .cart_item .woocommerce-Price-amount,
    .gg-header-minicart ul li .white-modal-cnt .fabio-minicart-price,
    .gg-header-minicart ul li .white-modal-cnt .fabio-minicart-remove a,
    .gg-header-minicart .fabio-minicart-subtotal,
    #simpleModal .close-window {
		color: <?php echo esc_html($gg_text_body_color); ?>;
	}
<?php } ?>


<?php if ($gg_headings_color != '#000000') { ?>
	h1,
	h2,
	h3,
	h4,
	h5,
	h6,
	.h1,
	.h2,
	.h3,
	.h4,
	.h5,
	.h6,
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th,
    body .wrap-forms label,
    label,
    body .wrap-forms input,
    body .wrap-forms select,
    .form-control,
    .select2-selection span,
    input.btn.btn-outline-black,
    a.btn.btn-outline-black,
    .btn-default:focus,
    .btn-default.focus,
    .btn-default:hover,
    input.btn-primary:hover,
    a.btn-primary:hover,
    .btn-secondary,
    .top-bar .top-header > li.shop .wishlist a:hover,
    .top-bar .cart-shop-top span.gg-products-count,
    #main-menu li a,
    .navbar-secondary #secondary-menu li a:hover,
    .page-meta-wrapper h1,
    .fabio-new-items-wrapper .fabio-categories a:hover,
    .fabio-new-items-wrapper .fabio-categories a.current,
    .fabio-new-items-wrapper .fabio-collection-categories a:hover,
    .fabio-new-items-wrapper .fabio-collection-categories a.current,
    .posts1 .fabio-post1-categories a:hover,
    .posts2 .fabio-post1-categories a:hover,
    .posts1 .fabio-post1-categories a.active,
    .posts2 .fabio-post1-categories a.active,
    .instagram .instagram-right .followers,
    .instagram input.btn-outline,
    .instagram a.btn-outline,
    .footer-social-icons-widget li a:hover,
    .contact-us-module-wrapper,
    .fabio_mailchimp.light .email input,
    .fabio_mailchimp.light .email input:focus,
    .fabio_mailchimp.light .email input:active,
    .fabio_mailchimp.light .signup .btn,
    body .fw-testimonials-2 .fw-testimonials-author .author-name,
    .entry-content a.more-link,
    .unyson-post-excerpt p a span,
    .gg-widget .tagcloud a,
    .posts1 .fabio-post1-categories a.active,
    .posts2 .fabio-post1-categories a.active,
    .posts1 .fabio-post1-categories a:hover,
    .posts2 .fabio-post1-categories a:hover,
    .post1-item .post1-image .overlay .title,
    .fabio_countify .countify_before,
    .fabio_countify .countify_after,
    .fabio_countify .countify_number,
    .fabio_countify .countify_label,
    .single-post .fabio-single-header .single_left_header_pagination a:hover,
    .single-post .fabio-single-header .single_right_header_pagination a:hover,
    blockquote:after,
    .post-social ul li a,
    .comments-area .media-list .media .media-body h4 a,
    .fabio-gallery-info .dl-horizontal dt,
    .fabio-gallery-info .dl-horizontal dd,
    .pagination>li>a:hover,
    .pagination>li>span:hover,
    .pagination>li>a:focus,
    .pagination>li>span:focus,
    div.pagination ul.pagination li a:hover,
    div.pagination ul.pagination li a:active,
    div.pagination ul.pagination li a:focus,
    div.pagination ul.pagination li.current a,
    .widget_price_filter .ui-slider .ui-slider-handle,
    .widget_price_filter .price_slider_amount .button,
    .u-columns .forgot-remember  .rememberme,
    .collection-carousel-pagination .next,
    .collection-carousel-pagination .prev,
    #secondary-menu .mega-menu .mega-menu-col > a,
    body.woocommerce-checkout .fabio-checkout .woocommerce-info a,
    .fabio-faq .faq-title a,
    .fabio-h1-icon,
    #reviews #comments .comment p.meta strong,
    #reviews #comments .comment .star-rating,
    .woocommerce select.fabio_woo_result_count,
    .woocommerce form.woocommerce-ordering select,
    .woocommerce .products-per-page select,
    .product-body > a > h3,
    .product-body > a > h2,
    .product-image .overlay .wrapper a,
    .products-carousel-nav .next:hover,
    .products-carousel-nav .prev:hover,
    .woocommerce .product .summary .variations td.label,
    .woocommerce-tabs .tabs li.active a,
    body.woocommerce #content table.wishlist_table.cart a.remove:hover,
    .woocommerce .shop_table.cart .product-name .product-remove a.remove:hover,
    .woocommerce dl.variation dt,
    .woocommerce .cart-collaterals .cart_totals table tr td,
    .woocommerce .cart-collaterals .cart_totals table tr td input,
    .woocommerce .cart-collaterals .cross-sells h2,
    .woocommerce .shop_table.cart td.actions .cart-collaterals .cart_totals th,
    .woocommerce .shop_table.cart td.actions .cart-collaterals .cart_totals td,
    .woocommerce .shop_attributes th  {
		color: <?php echo esc_html($gg_headings_color); ?>;
	}

    body .wrap-forms input:-moz-placeholder,
    body .wrap-forms textarea:-moz-placeholder {
        color: <?php echo esc_html($gg_headings_color); ?>;
    }

    body .wrap-forms input::-moz-placeholder,
    body .wrap-forms textarea::-moz-placeholder {
        color: <?php echo esc_html($gg_headings_color); ?>;
    }

    body .wrap-forms input:-ms-input-placeholder,
    body .wrap-forms textarea:-ms-input-placeholder {
        color: <?php echo esc_html($gg_headings_color); ?>;
    }

    body .wrap-forms input::-webkit-input-placeholder,
    body .wrap-forms textarea::-webkit-input-placeholder {
        color: <?php echo esc_html($gg_headings_color); ?>;
    }

<?php } ?>

<?php if ($gg_link_color != '#121416') { ?>
	a,
	a:hover,
	a:focus,
    .top-bar .logo-wrapper h2,
    .top-bar .logo-wrapper div,
    .top-bar .logo-wrapper h2 a,
    .top-bar .logo-wrapper .site-title a,
    .top-bar .cart-shop-top,
    body .fw-accordion .fw-accordion-title,
    .fw-special-title,
    .fabio_mailchimp .signup .btn,
    body .fw-btn-1,
    body .fw-iconbox-1 .fw-iconbox-image,
    aside.sidebar-nav h4,
    .fabio-posts-widget ul li .cnt .title a,
    .woocommerce .cart .quantity input.qty,
    .woocommerce .product .summary div[itemprop="offers"],
    .woocommerce .product .woocommerce-variation-price,
    .woocommerce .shop_table.cart th,
    .woocommerce .woocommerce-MyAccount-content p a {
		color: <?php echo esc_html($gg_link_color); ?>;
    }

<?php } ?>

<?php
/* Primary color */
if ( $gg_primary_color != '#000000') :
?>	
	input.btn-outline:hover,
    a.btn-outline:hover,
    .btn-default,
    .wc-proceed-to-checkout .checkout-button,
    .woocommerce .product .single_add_to_cart_button.button,
    input[type=submit],
    .btn-primary,
    .btn-secondary:hover,
    .woocommerce .product .single_add_to_cart_button.button:hover,
    .navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus,
    .navbar-default .navbar-toggle .icon-bar,
    .navbar-nav .mega-menu .sub-menu,
    .navbar-nav .dropdown-menu,
    .navbar-nav .sub-menu,
    .top-bar-flags .dropdown-menu,
    .nav .open>a,
    .nav .open>a:hover,
    .nav .open>a:focus,
    .fabio_mailchimp,
    body .fw-btn-1:hover,
    body .fw-btn-1:focus,
    .posts2 .gg_posts_grid .entry-title .blogpost-type,
    .widget_price_filter .ui-slider .ui-slider-handle,
    .sidebar-nav ul li:hover:after,
    .sidebar-nav ol li:hover:after,
    .sidebar-nav ul li.current-cat:after,
    .sidebar-nav ol li.current-cat:after,
    .sidebar-nav ul li.chosen:after,
    .sidebar-nav ol li.chosen:after,
    p.demo_store,
    span.onsale,
    span.soldout,
    .woocommerce .cart .quantity input.minus:hover,
    .woocommerce .cart .quantity input.plus:hover,
    .product-image-wrapper.inverse h3 span,
    .fabio-woo-cart-update .btn:hover,
    .woocommerce div.product div.images .woocommerce-product-gallery__trigger:after {
		background: <?php echo esc_html($gg_primary_color); ?>;
	}

    input.btn.btn-outline-black,
    a.btn.btn-outline-black,
    .woocommerce .product .single_add_to_cart_button.button:hover,
    .btn-default:focus,
    .btn-default.focus,
    .btn-default:hover,
    input.btn-primary:hover,
    a.btn-primary:hover,
    .navbar-default .navbar-toggle,
    .instagram input.btn-outline,
    .instagram a.btn-outline,
    .instagram input.btn-outline:hover,
    .instagram a.btn-outline:hover,
    .fabio-categories .product_ajax_loader,
    .fabio-collection-categories .product_ajax_loader,
    .fabio-post1-categories .gallery_ajax_loader,
    .fabio-post1-categories .posts_ajax_loader,
    .gg-ajax-loading,
    .ajaxLoadMore span,
    .fabio_mailchimp.light .signup .btn,
    .fabio_countify .countify-label-wrapper,
    .fabio_countify .countify_label,
    .post-social ul li a,
    .post-social ul li a.btn-outline:hover,
    .woocommerce div.product div.images .woocommerce-product-gallery__trigger:before,
    .woocommerce .product .single_add_to_cart_button.button,
    .woocommerce .product .single_add_to_cart_button.button:hover,
    .wc-proceed-to-checkout .checkout-button:hover {
        border-color: <?php echo esc_html($gg_primary_color); ?>;
    }

<?php endif; ?>

<?php
/* Secondary color */
if ($gg_secondary_color != '#f2f6f7') : ?>	
    
    .content-wrapper,
    .site-footer,
    .sidebar-nav ul li:after,
    .sidebar-nav ol li:after {
		background: <?php echo esc_html($gg_secondary_color); ?>;
	}

    .spinner > div {
        background: <?php echo esc_html($gg_secondary_color); ?> !important;
    }

    .woocommerce form.checkout #customer_details .right .wrapper:after {
        background: -webkit-linear-gradient(-45deg, <?php echo esc_html($gg_secondary_color); ?> 11px, transparent 0), -webkit-linear-gradient(-315deg, <?php echo esc_html($gg_secondary_color); ?> 11px, transparent 0);
        background: linear-gradient(-45deg, <?php echo esc_html($gg_secondary_color); ?> 11px, transparent 0), linear-gradient(45deg, <?php echo esc_html($gg_secondary_color); ?> 11px, transparent 0);
    }
	
<?php endif; ?>