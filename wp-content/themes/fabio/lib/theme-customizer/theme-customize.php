<?php
//Inline custom_css print
if (!function_exists('fabio_generate_dynamic_css')):
	function fabio_generate_dynamic_css() {
		/** Capture CSS output **/
		ob_start();// Capture all output into buffer
		require(get_template_directory() . '/lib/theme-customizer/custom_css.php');
		$css = ob_get_clean();// Store output in a variable, then flush the buffer
		$css = strip_tags($css);
		$css = preg_replace("!\s{2,}+!","",$css);
	
		return $css;
	}
endif;

?>