<?php
/*
 * Remove default stylesheet
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Suppress certain WooCommerce admin notices
 */
function fabio_wc_suppress_nags() {
    if ( class_exists( 'WC_Admin_Notices' ) ) {
        // Remove the "you have outdated template files" nag
        WC_Admin_Notices::remove_notice( 'template_files' );
        
        // Remove the "install pages" and "wc-install" nag
        WC_Admin_Notices::remove_notice( 'install' );
    }
}
add_action( 'wp_loaded', 'fabio_wc_suppress_nags', 99 );

/**
 * Query WooCommerce Extension Activation.
 *
 * @param string $extension Extension class name.
 * @return boolean
 */
function is_woocommerce_extension_activated( $extension = 'WC_Bookings' ) {
    return class_exists( $extension ) ? true : false;
}



/**
 * Load JavaScript for WC
 */
add_action('wp_enqueue_scripts', 'fabio_wc_scripts_loader');
function fabio_wc_scripts_loader() {
    wp_enqueue_script('woo-inputs', get_template_directory_uri() . '/js/woocommerce.js', array('jquery'), FABIO_THEMEVERSION, true); 
    wp_localize_script( 'woo-inputs', 'ajax_wc_object', array( 
        'ajax_url' => admin_url( 'admin-ajax.php' ),
        'myaccount_page_url' => get_permalink( get_option('woocommerce_myaccount_page_id') ),
    ));

    /**
     * Composite Products
     */
    if ( is_woocommerce_extension_activated( 'WC_Composite_Products' ) ) {
        wp_enqueue_style( 'fabio-woocommerce-composite-products-style', get_template_directory_uri() . '/styles/woocommerce/extensions/composite-products.css', 'storefront-woocommerce-style' );
    }
}

/**
 * Define image sizes - filter
 */

function fabio_filter_single_catalog_wc_image_size( array $size = array() ){
    $size = array(
        'width'  => '9999',
        'height' => '9999',
        'crop'   => 1
    );
    return $size;
}

function fabio_filter_thumbnail_wc_image_size( array $size = array() ){
    $size = array(
        'width'  => '100',
        'height' => '150',
        'crop'   => 1
    );
    return $size;
}

// single
add_filter( 'woocommerce_get_image_size_shop_single', 'fabio_filter_single_catalog_wc_image_size' );
// catalog
add_filter( 'woocommerce_get_image_size_shop_catalog', 'fabio_filter_single_catalog_wc_image_size' );
// thumbnail
add_filter( 'woocommerce_get_image_size_shop_thumbnail', 'fabio_filter_thumbnail_wc_image_size' );

//Remove the filters at user input
if ( _get_field('gg_activate_product_image_sizes','option', false) === true ) {
    // single
    remove_filter( 'woocommerce_get_image_size_shop_single', 'fabio_filter_single_catalog_wc_image_size' );
    // catalog
    remove_filter( 'woocommerce_get_image_size_shop_catalog', 'fabio_filter_single_catalog_wc_image_size' );
    // thumbnail
    remove_filter( 'woocommerce_get_image_size_shop_thumbnail', 'fabio_filter_thumbnail_wc_image_size' );
}


/**
 * WooCommerce Breadcrubs
 */
function fabio_wc_breadcrumbs() {
    return array(
        'delimiter'   => ' <span class="delimiter">X</span> ',
        'wrap_before' => '<div class="gg-breadcrumbs"><i class="icon_house_alt"></i> ',
        'wrap_after'  => '</div>',
        'before'      => '',
        'after'       => '',
        'home'        => _x('Home', 'breadcrumb', 'fabio'),
    );
}
add_filter( 'woocommerce_breadcrumb_defaults', 'fabio_wc_breadcrumbs' );


/**
 * Add Sold out badge
 */
include (get_template_directory().'/lib/woocommerce-sold-out.php');


/**
 * Hide shop page title
 */
add_filter('woocommerce_show_page_title', 'fabio_remove_shop_title' );
function fabio_remove_shop_title() {
    return false;
}

/** 
 * Remove product rating display on product loops
 */
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

/** 
 * Remove product rating display on product single
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );


/*
 * Add custom pagination
 */
function fabio_wc_pagination() {
    fabio_pagination();       
}
remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);
add_action( 'woocommerce_after_shop_loop', 'fabio_wc_pagination', 10);

/**
 * Move price bellow short description
 */



/*
 * Allow shortcodes in product excerpts
 */
if (!function_exists('woocommerce_template_single_excerpt')) {
   function woocommerce_template_single_excerpt( $post ) {
       global $post;
       if ($post->post_excerpt) echo '<div itemprop="description">' . do_shortcode(wpautop(wptexturize($post->post_excerpt))) . '</div>';
   }
}

/*
 * Shop page - Number of products per row
 */
add_filter('loop_shop_columns', 'fabio_shop_columns');
if (!function_exists('fabio_shop_columns')) {
    function fabio_shop_columns() {
        return _get_field('gg_shop_product_columns','option', '3');
    }
}

/*
 * Shop page - Number of products per page
 */
add_filter('loop_shop_per_page',  'fabio_shop_products_per_page', 20);
if (!function_exists('fabio_shop_products_per_page')) {
    function fabio_shop_products_per_page() {
        $product_per_page = _get_field('gg_product_per_page','option', '12');
        return $product_per_page;
    }
}


/**
 * Enable/Disable Sale Flash
 */
if ( _get_field('gg_store_sale_flash','option', true) === true ) {
    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
    add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
} else {
    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
}

/**
 * Enable/Disable Products price
 */


/**
 * Enable/Disable Add to cart
 */
if ( _get_field('gg_store_add_to_cart','option', true) === true ) {
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
    add_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_add_to_cart',30);
} else {
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart',10);
}

/**
 * Options for product page
 */

/*Sale flash*/
if ( _get_field('gg_product_sale_flash','option', true) === false ) {
    remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
}

/*Price*/
if ( _get_field('gg_product_products_price','option', true) === false ) {
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25);
}

/*Product summary*/
if ( _get_field('gg_product_products_excerpt','option', true) === false ) {
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
} 

/*Add to cart*/
if ( _get_field('gg_product_add_to_cart','option', true) === false ) {
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
}

/*Meta*/
if ( _get_field('gg_product_products_meta','option', true) === false ) {
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
    
}

//Tabs

//Disable reviews tab
if ( _get_field('gg_product_reviews_tab','option', false) === false ) {
    /**
     * Remove Review tab
     */
    add_filter( 'woocommerce_product_tabs', 'fabio_product_remove_reviews_tab', 98);
    function fabio_product_remove_reviews_tab($tabs) {
        unset($tabs['reviews']);
        return $tabs;
    }
}
//Disable attributes tab
if ( _get_field('gg_product_attributes_tab','option', true) === false ) {
    
    /**
     * Remove Attributes tab
     */
    add_filter( 'woocommerce_product_tabs', 'fabio_product_remove_attributes_tab', 98);
    function fabio_product_remove_attributes_tab($tabs) {
        unset($tabs['additional_information']);
        return $tabs;
    }
}
//Disable description tab
if ( _get_field('gg_product_description_tab','option', true) === false ) {
    /**
     * Remove Description tab
     */
    add_filter( 'woocommerce_product_tabs', 'fabio_product_remove_description_tab', 98);
    function fabio_product_remove_description_tab($tabs) {
        unset($tabs['description']);
        return $tabs;
    }
}


/**
 * Enable/Disable Related products
 */
if ( _get_field('gg_product_related_products','option', true) === true ) {
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
    add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products', 20);

    add_filter( 'woocommerce_output_related_products_args', 'fabio_related_products_args' );
      function fabio_related_products_args( $args ) {
        $args['posts_per_page'] = 4; // 4 related products
        $args['columns'] = 4; // arranged in 2 columns
        return $args;
    }

} else {
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
}

/**
 * Enable/Disable Up Sells products
 */
if ( _get_field('gg_product_upsells_products','option', true) === true ) {
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
    add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display', 15 );

    if ( ! function_exists( 'woocommerce_upsell_display_output' ) ) {
        function woocommerce_upsell_display_output() {
            woocommerce_upsell_display( 2,2 ); // Display 2 products in rows of 2
        }
    }

} else {
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
}


/**
 * Enable/Disable Cross Sells products
 */
if ( _get_field('gg_product_crosssells_products','option', true) === true ) {
    remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
    add_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_output',5 );

    if ( ! function_exists( 'woocommerce_cross_sell_output' ) ) {
        function woocommerce_cross_sell_output() {
            woocommerce_cross_sell_display( 1,1 ); // Display 2 products in rows of 2
        }
    }
} else {
    remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
}

/**
 * Catalog mode functions (must be always the last function)
 */
if ( _get_field('gg_store_catalog_mode','option', false ) === true ) {
    // Remove add to cart button from the product loop
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart',10);
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
    remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_add_to_cart',30);

    // Remove add to cart button from the product details page
    remove_action( 'woocommerce_before_add_to_cart_form', 'woocommerce_template_single_product_add_to_cart', 10, 2);
    
    add_filter( 'woocommerce_add_to_cart_validation', '__return_false', 10, 2 );

    // check for clear-cart get param to clear the cart
    add_action( 'init', 'fabio_wc_clear_cart_url' );
    function fabio_wc_clear_cart_url() {    
        global $woocommerce;
        if ( isset( $_GET['empty-cart'] ) ) { 
            $woocommerce->cart->empty_cart(); 
        }  
    }

    add_action( 'wp', 'fabio_check_pages_redirect');
    function fabio_check_pages_redirect() {
        $cart     = is_page( wc_get_page_id( 'cart' ) );
        $checkout = is_page( wc_get_page_id( 'checkout' ) );

        if ( $cart || $checkout ) {
            wp_redirect( esc_url( home_url( '/' ) ) );
            exit;
        }
    }
    
}

/**
 * Remove product category description - its included in page_header function
 **/
remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );


/*WooCommerce 3.0*/
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
		
//Breadcrumbs
function fabio_woocommerce_before_single_product(){
    fabio_breadcrumbs(); 
}		
add_action('woocommerce_before_single_product', 'fabio_woocommerce_before_single_product');	
	
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 ); 
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 ); 


add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price',  5 ); 
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 10 ); 
add_action( 'woocommerce_single_product_summary', 'fabio_woocommerce_social_icons', 30 ); 

function fabio_woocommerce_social_icons(){
	get_template_part('parts/part','socialshare-with-navigation');
}

if ( class_exists('YITH_WCWL') ) {
    //Set to display the btn as shortcode
    update_option( 'yith_wcwl_button_position', 'shortcode' );
}