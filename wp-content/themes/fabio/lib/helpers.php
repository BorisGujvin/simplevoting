<?php
// Verify if woocommerce is active
if ( ! function_exists( 'fabio_is_wc_activated' ) ) {
	function fabio_is_wc_activated() {
		if ( class_exists( 'woocommerce' ) ) {
			return true;
		} else {
			return false;
		}
	}
}

// Verify if WPML is active
if ( ! function_exists( 'fabio_is_wpml_activated' ) ) {
	function fabio_is_wpml_activated() {
		if ( class_exists( 'SitePressLanguageSwitcher' ) ) {
			return true;
		} else {
			return false;
		}
	}
}

if ( fabio_is_wpml_activated() ) {
  //Disable WPML styles
  define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
  define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
  define('ICL_DONT_LOAD_LANGUAGES_JS', true);
}

// Hide activation and update specific parts of Slider Revolution
if( function_exists('set_revslider_as_theme') ) {

	add_action( 'init', 'fabio_rev_slider' );
	function fabio_rev_slider() {
		set_revslider_as_theme();
	}

}

//Page container
if (! function_exists('fabio_page_container')) :
function fabio_page_container( $post_id = NULL ) {

		global $post, $product;

		if ( $post_id != FALSE || $post_id == 'special_page' ) {
			$post = get_post( $post_id );
		}

		//Get page layout options
		if ( is_search() ) {
			$page_layout = _get_field('gg_search_page_layout', 'option', 'no_sidebar');
		} elseif ( fabio_is_wc_activated() && ( is_shop() || is_product_category() ) ) {
			$page_layout = _get_field('gg_page_layout_select', get_option( 'woocommerce_shop_page_id' ), 'no_sidebar');
		} elseif ( is_archive() || is_category() || is_tag() ) {
			$page_layout = _get_field('gg_cat_page_layout', 'option', 'no_sidebar');
		} elseif ( is_singular('product') ) {
			$page_layout = 'no_sidebar';
			//$page_layout = _get_field('gg_page_layout_select', $product->ID, 'no_sidebar');
		} else {
			$page_layout = _get_field('gg_page_layout_select', $post->ID, 'no_sidebar');
		}


  switch ($page_layout) {
	  case "with_right_sidebar":
		  $page_content_class = 'col-xs-12 col-md-9 pull-left';
		  break;
	  case "with_left_sidebar":
		  $page_content_class = 'col-xs-12 col-md-9 pull-right';
		  break;
	  case "no_sidebar":
		  $page_content_class = 'col-xs-12 col-md-12';
		  break;
	  case NULL:
		  $page_content_class = 'col-xs-12 col-md-9 pull-left';
		  break;
  }

  echo esc_attr($page_content_class);
}
endif;

//Page sidebar
if (! function_exists('fabio_page_sidebar')) :
function fabio_page_sidebar( $post_id = NULL ) {

		global $post, $product;

		if ( $post_id != FALSE || $post_id == 'special_page' ) {
			$post = get_post( $post_id );
		}

		//Get page layout options
		if ( is_search() ) {
			$page_layout = _get_field('gg_search_page_layout', 'option','no_sidebar');
		} elseif ( fabio_is_wc_activated() && ( is_shop() || is_product_category() ) ) {
			$page_layout = _get_field('gg_page_layout_select', get_option( 'woocommerce_shop_page_id' ), 'no_sidebar');
		} elseif ( is_archive() || is_category() || is_tag() ) {
			$page_layout = _get_field('gg_cat_page_layout', 'option','no_sidebar');
		} elseif ( is_singular('product') ) {
			$page_layout = 'no_sidebar';
			//$page_layout = _get_field('gg_page_layout_select', $product->ID, 'no_sidebar');
		} else {
			$page_layout = _get_field('gg_page_layout_select', $post->ID, 'no_sidebar');
		}

  switch ($page_layout) {
	  case "with_right_sidebar":
		  $page_sidebar_class = 'col-xs-12 col-md-3 pull-right';
		  break;
	  case "with_left_sidebar":
		   $page_sidebar_class = 'col-xs-12 col-md-3 pull-left';
		  break;
	  case "no_sidebar":
		  $page_sidebar_class = '';
		  break;
	  case NULL:
		  $page_sidebar_class = 'col-xs-12 col-md-3 pull-right';
		  break;
  }

  if ($page_layout !== 'no_sidebar') {
  ?>
  <div class="<?php echo esc_attr($page_sidebar_class); ?>">
	  <aside class="sidebar-nav">
		  <?php get_sidebar(); ?>
	  </aside>
	  <!--/aside .sidebar-nav -->
  </div><!-- /.col-3 col-sm-3 col-lg-3 -->
  <?php } ?>
<?php }
endif;


//Get the global page id
if (! function_exists('fabio_global_page_id')) :
function fabio_global_page_id() {

  global $wp_query;

  if ( fabio_is_wc_activated() && is_shop() ) {
	$current_page_id = get_option( 'woocommerce_shop_page_id' );
  // If there is a post
  } elseif ( is_single() || ( is_home() && !is_front_page() ) || ( is_page() && !is_front_page() ) ) {
	$current_page_id = $wp_query->post->ID;

  } else {
	$current_page_id = '';
  }

  if(is_home()){
	  if('page' == get_option('show_on_front')){
		  if(is_front_page()){
			  $current_page_id = get_option('page_on_front');
		  }else{
			  $current_page_id = get_option('page_for_posts');
		  }
	  }
  }

  return $current_page_id;

}
endif;

if (! function_exists('fabio_wp_title')) :
function fabio_wp_title( $display = true ) {
  global $page, $paged;

  $search = get_query_var('s');
  $title = '';

  // If there is a post
  if ( is_single() || ( is_home() && !is_front_page() ) || ( is_page() && !is_front_page() ) ) {
	$title = the_title();
  }

  // If there's a post type archive
  if ( is_post_type_archive() ) {
	$post_type = get_query_var( 'post_type' );
	if ( is_array( $post_type ) )
	  $post_type = reset( $post_type );
	$post_type_object = get_post_type_object( $post_type );
	if ( ! $post_type_object->has_archive )
		$title = single_term_title( '', false );

  }

  // If there's a category or tag
  if ( is_category() || is_tag() ) {
	$title = single_term_title( '', false );
  }

    // If there's a taxonomy
    if ( is_tax() ) {
    	$term = get_queried_object();
    	if ( $term ) {
            $tax = get_taxonomy( $term->taxonomy );
            if ( defined('WPSEO_VERSION') ) {
                $title = single_term_title( $tax->labels->name, false );
            } else {
                $title = single_term_title( false );
            }

						$title = single_cat_title();
    	}
    }

  // If there's an author
  if ( is_author() && ! is_post_type_archive() ) {
	$author = get_queried_object();
	if ( $author )
	  $title = $author->display_name;
  }

  // Post type archives with has_archive should override terms.
  if ( is_post_type_archive() && $post_type_object->has_archive )
	$title = single_term_title( '', false );

  if ( is_year() || is_month() || is_day() ) {
	$title = get_the_archive_title();
  }

  // If it's a search
  if ( is_search() ) {
	/* translators: 1 search phrase */
	$title = sprintf(esc_html__('Search Results For: %1$s','fabio'), strip_tags($search));
  }

  if ( fabio_is_wc_activated() && (is_shop()) ) {
	$title = get_the_title(wc_get_page_id( 'shop' ));
  }

  if ( fabio_is_wc_activated() && is_page() && is_wc_endpoint_url() ) {
	$endpoint = WC()->query->get_current_endpoint();
	if ( $endpoint_title = WC()->query->get_endpoint_title( $endpoint ) ) {
		$title = $endpoint_title;
	}
  }

  // /**
  //  * Filter the text of the page title.
  //  *
  //  * @since 2.0.0
  //  *
  //  * @param string $title       Page title.
  //  */
  // $title = apply_filters( 'wp_title', $title );

  // Send it out
    if ( $display )
        echo $title;
    else

    return $title;

}

endif;

/**
 * Display an optional post thumbnail.
 */
if ( ! function_exists( 'fabio_post_thumbnail' ) ) :
function fabio_post_thumbnail() {
  if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
	return;
  }

  if ( is_single() ) :
  ?>

  <div class="post-thumbnail">
	<?php the_post_thumbnail(); ?>
  </div><!-- .post-thumbnail -->

  <?php else : ?>

  <a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
	<?php
	  the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
	?>
  </a>

  <?php endif; // End is_singular()
}
endif;

/**
 * Add class to next post link
 */
function fabio_next_posts_link_css($content) {
	return 'class="btn btn-primary"';
}
add_filter('next_posts_link_attributes', 'fabio_next_posts_link_css' );


/**
 * Excerpt read more
 *
 */

function fabio_excerpt_more( $more ) {
	return '<br class="read-more-spacer"> <a class="more-link" href="'. esc_url(get_permalink( get_the_ID() )) . '">' . esc_html__('Read More', 'fabio') . '</a>';
}
add_filter( 'excerpt_more', 'fabio_excerpt_more' );

/**
 * Tags widget style
 *
 */
function fabio_tag_cloud_widget($args) {
	$args['number'] = 0; //adding a 0 will display all tags
	$args['largest'] = 11; //largest tag
	$args['smallest'] = 11; //smallest tag
	$args['unit'] = 'px'; //tag font unit
	$args['format'] = 'list'; //ul with a class of wp-tag-cloud
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'fabio_tag_cloud_widget' );

/**
 * Display template for pagination.
 *
 */
if (!function_exists('fabio_pagination')) :
function fabio_pagination($query=null) {


	$prev_arrow = is_rtl() ? '<i class="fa  fa-angle-right"></i>' : '<i class="fa fa-angle-left"></i>';
	$next_arrow = is_rtl() ? '<i class="fa   fa-angle-left"></i>' : '<i class="fa fa-angle-right"></i>';

	global $wp_query;
	$query = $query ? $query : $wp_query;
	$total = $query->max_num_pages;
	$big = 999999999; // need an unlikely integer

	if( !$current_page = get_query_var('paged') )
	  $current_page = 1;
	if( get_option('permalink_structure') ) {
	  $format = 'page/%#%/';
	} else {
	  $format = '&paged=%#%';
	}

	$paginate_links = paginate_links(array(
	  'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	  'format'    => $format,
	  'current'   => max( 1, get_query_var('paged') ),
	  'total'     => $total,
	  'mid_size'    => 3,
	  'type'      => 'list',
	  'prev_text'   => $prev_arrow,
	  'next_text'   => $next_arrow,
	 ) );

	$paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class='pagination'>", $paginate_links );
	$paginate_links = str_replace( '<li><span class="page-numbers dots">', "<li><a href='#'>", $paginate_links );
	$paginate_links = str_replace( "<li><span class='page-numbers current'>", "<li class='current'><a href='#'>", $paginate_links );
	$paginate_links = str_replace( '</span>', '</a>', $paginate_links );
	$paginate_links = str_replace( "<li><a href='#'>&hellip;</a></li>", "<li><span class='dots'>&hellip;</span></li>", $paginate_links );
	$paginate_links = preg_replace( '/\s*page-numbers/', '', $paginate_links );
	// Display the pagination if more than one page is found
	if ( $paginate_links ) {
	  echo '<div class="pagination">';
	  echo wp_kses_post($paginate_links);
	  echo '</div><!--// end .pagination -->';
	}

}
endif;

/**
 * Display template for comments and pingbacks.
 *
 */
if (!function_exists('fabio_comment')) :
	function fabio_comment($comment, $args, $depth)
	{
		$GLOBALS['comment'] = $comment;
		switch ($comment->comment_type) :
			case 'pingback' :
			case 'trackback' : ?>

				<li <?php comment_class('media, comment'); ?> id="comment-<?php comment_ID(); ?>">
					<div class="media-body">
						<p>
							<?php esc_html_e('Pingback:', 'fabio'); ?> <?php comment_author_link(); ?>
						</p>
					</div><!--/.media-body -->
				<?php
				break;
			default :
				// Proceed with normal comments.
				global $post; ?>

				<li <?php comment_class('media'); ?> id="li-comment-<?php comment_ID(); ?>">
						<a href="<?php echo esc_url($comment->comment_author_url); ?>" class="pull-left avatar-holder">
							<?php echo get_avatar($comment, 70); ?>
						</a>
						<div class="media-body">
							<h4 class="media-heading comment-author vcard">
								<?php
								printf('<cite class="fn">%1$s %2$s</cite>',
									get_comment_author_link(),
									// If current post author is also comment author, make it known visually.
									($comment->user_id === $post->post_author) ? '<span class="label"> ' . esc_html__(
										'Post author',
										'fabio'
									) . '</span> ' : ''); ?>
							</h4>
							<p class="meta">
								<?php printf('<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
										esc_url(get_comment_link($comment->comment_ID)),
										get_comment_time('c'),
										sprintf(
											esc_html__('%1$s at %2$s', 'fabio'),
											get_comment_date(),
											get_comment_time()
										)
									); ?> <span class="replay-button"><?php comment_reply_link( array_merge($args, array(
											'reply_text' => ' / <i class="arrow_back"></i> '. esc_html__('Reply', 'fabio'),
											'depth'      => $depth,
											'max_depth'  => $args['max_depth']
										)
									)); ?></span>
							</p>

							<?php if ('0' == $comment->comment_approved) : ?>
								<p class="comment-awaiting-moderation"><?php esc_html_e(
									'Your comment is awaiting moderation.',
									'fabio'
								); ?></p>
							<?php endif; ?>

							<?php comment_text(); ?>

						</div>
						<!--/.media-body -->
				<?php
				break;
		endswitch;
	}
endif;


/**
 * WPML Multi currency function
 **/
if ( ! function_exists('fabio_currency_switcher') ) {
	function fabio_currency_switcher() {
	ob_start();
	if ( class_exists('woocommerce_wpml') && fabio_is_wc_activated() ) {
	?>

	<a href="" data-toggle="dropdown" aria-haspopup="true" class="dropdown-toggle">
		<span><?php echo get_woocommerce_currency_symbol(); ?></span>
		<span class="hidden-sm hidden-xs hidden"><?php echo get_woocommerce_currency(); ?></span>
	</a>
	<?php do_action('currency_switcher', array('format' => '%symbol%')); ?>

	<?php
	}
	return ob_get_clean();
	}
}

/**
 * WPML - Language dropdown with flags
 **/

if ( ! function_exists('fabio_wpml_language_sel') ) {

	function fabio_wpml_language_sel(){
		if (fabio_is_wpml_activated()) {
			if (function_exists('icl_get_languages')) {
			  $languages = icl_get_languages('skip_missing=0&orderby=custom&order=asc');
			  if(!empty($languages)){
				  $out = '<div class="dropdown">';
				  $dropdown = '';
				  foreach ($languages as $lang) {
					  $lcode = explode('-',$lang['language_code']);

					  if ($lang['active']) {
						  $button = '<a href="#" data-toggle="dropdown" aria-haspopup="true" class="dropdown-toggle"><img src="'.esc_url($lang['country_flag_url']).'" alt="'.esc_html($lang['translated_name']).'" /><span class="hidden-sm hidden-xs">'.$lang['translated_name'].'</span></a>';
					  } else {
						  $dropdown .= '<li><a href="'.esc_url($lang['url']).'" title="'.esc_html($lang['native_name']).'"><img src="'.esc_url($lang['country_flag_url']).'" alt="'.esc_html($lang['translated_name']).'" /> '.$lang['translated_name'].' </a></li>';
					  }
				  }
				  $out .= $button;
				  $out .= '<ul id="langswitch" class="dropdown-menu noclose">'.$dropdown.'</ul>';
				  $out .= '</div>';
				  return $out;
			  }
			}

		}
	}

}
