<?php
add_action( 'tgmpa_register', 'fabio_register_required_plugins' );

function fabio_register_required_plugins() {
    $plugins = array(
        array(
            'name'               => 'OKThemes Fabio Shortcodes', // The plugin name
            'slug'               => 'fabio-shortcodes', // The plugin slug (typically the folder name)
            'source'             => get_template_directory() . '/plugins/fabio-shortcodes.zip', // The plugin source
            'required'           => true, // If false, the plugin is only 'recommended' instead of required
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'       => '', // If set, overrides default API URL and points to an external URL
            'version'            => '2.1',
        ),
        array(
            'name'               => 'OKThemes Fabio Social Share', // The plugin name
            'slug'               => 'fabio-social-share', // The plugin slug (typically the folder name)
            'source'             => get_template_directory() . '/plugins/fabio-social-share.zip', // The plugin source
            'required'           => false, // If false, the plugin is only 'recommended' instead of required
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'       => '', // If set, overrides default API URL and points to an external URL
            'version'            => '1.0',
        ),
        array(
            'name'               => 'Advanced Custom Fields Pro', // The plugin name
            'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name)
            'source'             => get_template_directory() . '/plugins/advanced-custom-fields-pro.zip', // The plugin source
            'required'           => true, // If false, the plugin is only 'recommended' instead of required
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'       => '', // If set, overrides default API URL and points to an external URL
            'version'            => '5.7.9',
        ),
        array(
            'name'               => 'Slider Revolution', // The plugin name
            'slug'               => 'revslider', // The plugin slug (typically the folder name)
            'source'             => get_template_directory() . '/plugins/revslider.zip', // The plugin source
            'required'           => false, // If false, the plugin is only 'recommended' instead of required
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'       => '', // If set, overrides default API URL and points to an external URL
            'version'            => '5.4.8.1',
        ),
        array(
            'name'      => 'WooCommerce',
            'slug'      => 'woocommerce',
            'required'  => false,
        ),
        array(
            'name'      => 'YITH WooCommerce Wishlist',
            'slug'      => 'yith-woocommerce-wishlist',
            'required'  => false,
        ),
        array(
            'name'     => 'MailChimp for WordPress',
            'slug'     => 'mailchimp-for-wp',
            'required' => false,
        )
    );

    $config = array(
        'id'           => 'fabio',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php',            // Parent menu slug.
        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '', 
    );
    tgmpa( $plugins, $config );
}
?>