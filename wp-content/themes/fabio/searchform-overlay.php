<?php
/**
 * Search Form Template
 *
 * @package WordPress
 * @subpackage fabio
 */
?>

<form method="get" id="searchformoverlay" class="" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input class="form-control" type="text" value="<?php the_search_query(); ?>" placeholder="<?php esc_attr_e( 'Search', 'fabio' ); ?>" name="s" id="s" />
	<input class="btn btn-primary" type="submit" id="searchsubmit" value="<?php esc_attr_e( 'Go', 'fabio' ); ?>" />
</form>

