<?php
/**
 * The template for displaying Archive pages.
 *
 * @package WordPress
 * @subpackage fabio
 */
get_header();

$site_has_blog_id = ( 'page' == get_option( 'show_on_front' ) ? get_option( 'page_for_posts' ) : '' );

if ( $site_has_blog_id ) {
   $blog_columns = _get_field('gg_blog_columns',$site_has_blog_id,3);
} else {
    $blog_columns = 3;
}

?>

<section id="content">
    <div class="container">
        <div class="row">
            <div class="<?php fabio_page_container('special_page'); ?>">

            <?php if (have_posts()) :
            // Queue the first post.
            the_post();
            // Rewind the loop back
            rewind_posts();
            ?>
            <div class="gg_posts_grid">
                <ul class="masonry_post el-grid post-arhive" data-layout-mode="fitRows" data-gap="gap" data-columns="1">
                
                <?php while (have_posts()) : the_post(); ?>

                    <li class="isotop-grid-item isotope-item post-item col-xs-12 col-md-<?php echo esc_attr(floor( 12 / $blog_columns )); ?>">
                        <?php get_template_part( 'parts/post-formats/part', get_post_format() ); ?>
                    </li>      

                <?php endwhile; ?>

                </ul>
            </div>

            <?php if (function_exists("fabio_pagination")) {
                fabio_pagination();
            } ?>

            <?php // If no content, include the "No posts found" template.
            else :
                get_template_part( 'parts/post-formats/part', 'none' );
            endif;
            ?>

            </div>
            <?php fabio_page_sidebar('special_page'); ?>

        </div><!-- .row -->
    </div><!-- .container -->    
</section>

<?php get_footer(); ?>