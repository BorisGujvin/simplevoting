<?php
/**
 * Add to wishlist button template
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Wishlist
 * @version 2.0.8
 */

if ( ! defined( 'YITH_WCWL' ) ) {
    exit;
} // Exit if accessed directly

global $product;

$tooltip_placement = 'bottom';
$icon = '<i class="fa fa-heart-o"></i>';
$label = '';

if (is_product($product_id)) {
    $tooltip_placement = 'top';
}

if ( $exists && ! $available_multi_wishlist ) {
    $icon = '<i class="fa fa-heart"></i>';
}

?>

<a href="<?php echo ( $exists && ! $available_multi_wishlist ) ? esc_url( $wishlist_url ) : esc_url( add_query_arg( 'add_to_wishlist', $product_id ) ) ?>" rel="nofollow" data-product-id="<?php echo $product_id ?>" data-product-type="<?php echo $product_type?>" class="<?php echo $link_classes ?> <?php echo ( $exists && ! $available_multi_wishlist ) ? 'already_in_wishlist' : '' ?>" data-toggle="tooltip" data-placement="<?php echo $tooltip_placement; ?>" title="<?php echo ( $exists && ! $available_multi_wishlist ) ? $already_in_wishslist_text  :  '' ?>">
    <?php echo $icon ?>
    <?php echo $label ?>
</a>
<img src="<?php echo esc_url( YITH_WCWL_URL . 'assets/images/wpspin_light.gif' ) ?>" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />