<?php
/**
 * Theme Functions
 *
 * @author Gogoneata Cristian <cristian.gogoneata@gmail.com>
 * @package WordPress
 * @subpackage fabio
 */

$theme = wp_get_theme();
if ( is_child_theme() ) {
    $theme = wp_get_theme( $theme->get( 'Template' ) );
}
$theme_version = $theme->get( 'Version' );

define("FABIO_THEMEVERSION",$theme_version);

// Include the helpers
include (get_template_directory().'/lib/helpers.php');

// Load plugins
require_once (get_template_directory() . '/lib/class-tgm-plugin-activation.php');
include (get_template_directory() . '/lib/register-tgm-plugins.php');

//ACF
if ( class_exists( 'acf' ) ) {

    // ACF functions
    include get_template_directory() . '/lib/acf/acf-functions.php';

    //ACF theme customizer
    include get_template_directory() . '/lib/theme-customizer/theme-customize.php';

    // Hide ACF field group menu item
    add_filter('acf/settings/show_admin', '__return_false');

    // Include text domain for metaboxes
    function fabio_acf_settings_textdomain( $export_textdomain ) {
        return 'fabio';
    }
    add_filter('acf/settings/export_textdomain', 'fabio_acf_settings_textdomain');

    // ACF metaboxes
    include get_template_directory() . '/lib/metaboxes.php';

}

// ACF fields
include get_template_directory() . '/lib/acf/acf-fields.php';

// Include sidebars
require_once (get_template_directory() . '/lib/sidebars.php');

// Include widgets
require_once (get_template_directory() . '/lib/widgets.php');

/**
 * Load woocommerce functions
 */
if ( fabio_is_wc_activated() ) {
    require_once get_template_directory() . '/lib/theme-woocommerce.php';
    require_once (get_template_directory() . '/lib/woocommerce_functions.php');
}

// Include breadcrumbs
include_once (get_template_directory() . '/lib/breadcrumbs.php');

// load custom walker menu class file
require (get_template_directory() . '/lib/nav/class-bootstrapwp_walker_nav_menu.php');

/**
 * Maximum allowed width of content within the theme.
 */
if (!isset($content_width)) {
    $content_width = 1170;
}

/**
 * Setup Theme Functions
 *
 */
if (!function_exists('fabio_theme_setup')):
    function fabio_theme_setup() {

        load_theme_textdomain('fabio', get_template_directory() . '/lang');

        add_theme_support( 'title-tag' );
        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'woocommerce' );
        add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
        add_theme_support( 'custom-header' ) ;
        $defaults = array(
            'default-color'          => 'ffffff',
            'default-image'          => '',
            'default-repeat'         => '',
            'default-position-x'     => '',
        );
        add_theme_support( 'custom-background', $defaults);

        register_nav_menus(
            array(
                'main-menu'      => esc_html__('Main Menu', 'fabio'),
                'secondary-menu' => esc_html__('Secondary Menu', 'fabio'),
                'footer-menu'    => esc_html__('Footer Menu', 'fabio'),
            )
        );

        set_post_thumbnail_size('full');
        add_image_size('fabio-nav-product-image', 150, 190, array( 'left', 'top' ));

    }
endif;
add_action('after_setup_theme', 'fabio_theme_setup');


/**
 * Default font loading
 */
if ( ! function_exists( 'fabio_fonts_url' ) ) :
    function fabio_fonts_url() {
        $fonts_url = '';
        $fonts     = array();
        $subsets   = 'latin,latin-ext';

        if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'fabio' ) ) {
            $fonts[] = 'Open Sans:300,400,600,700,800';
        }

        if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'fabio' ) ) {
            $fonts[] = 'Montserrat:100,200,300,400,500,600,700,800,900';
        }

        $subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'fabio' );

        if ( 'cyrillic' == $subset ) {
            $subsets .= ',cyrillic,cyrillic-ext';
        } elseif ( 'greek' == $subset ) {
            $subsets .= ',greek,greek-ext';
        } elseif ( 'devanagari' == $subset ) {
            $subsets .= ',devanagari';
        } elseif ( 'vietnamese' == $subset ) {
            $subsets .= ',vietnamese';
        }

        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), '//fonts.googleapis.com/css' );
        }

        return $fonts_url;
    }
endif;


/**
 * Load CSS styles for theme.
 *
 */
function fabio_styles_loader() {

    if ( fabio_is_wc_activated() ) {
    	wp_enqueue_style('fabio-component', get_template_directory_uri() . '/styles/component.css', false, FABIO_THEMEVERSION, 'all');
    }

    if ( function_exists('getGoogleFonts') ) {
        getGoogleFonts( get_the_ID() );
    }

    /*Register fonts if acf is not available*/
    if ( ! class_exists( 'acf' ) ) {
        // Add custom fonts, used in the main stylesheet.
        wp_enqueue_style( 'fabio-google-fonts', fabio_fonts_url(), array(), null );
    }

    wp_enqueue_style('fabio-bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css', false, FABIO_THEMEVERSION, 'all');
    wp_enqueue_style('fabio-font-awesome', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css', false, FABIO_THEMEVERSION, 'all');
    wp_enqueue_style('fabio-arrows-font', get_template_directory_uri() . '/assets/arrows-font/styles.css', false, FABIO_THEMEVERSION, 'all');

    wp_enqueue_style('fabio-magnific-popup', get_template_directory_uri() . '/styles/magnific-popup.css', false, FABIO_THEMEVERSION, 'all');

    //SlickCarousel
    wp_enqueue_style('fabio-slick', get_template_directory_uri() . '/assets/slick/slick.css', false, FABIO_THEMEVERSION, 'all');

    if ( fabio_is_wc_activated() ) {
        wp_enqueue_style('fabio-woocommerce', get_template_directory_uri() . '/styles/gg-woocommerce.css', false, FABIO_THEMEVERSION, 'all');
    }

    //Default stylesheet
    wp_enqueue_style('fabio-style', get_stylesheet_uri() );

    //Generated CSS from Unyson
	if( function_exists('fabio_get_cssUrlManager_path') ) {

		wp_enqueue_style('fabio-generated', fabio_get_cssUrlManager_path() . 'generated.css', false, FABIO_THEMEVERSION, 'all');

		$post_id = get_the_ID();
		if( get_post_meta( get_the_ID(), 'fabio_has_css_file', true ) == 1){
			wp_enqueue_style('fabio-generated-'.$post_id ,fabio_get_cssUrlManager_path() .'generated-'.$post_id.'.css', false, FABIO_THEMEVERSION, 'all');
		}
	}

    //Custom styling from theme options Colors and Custom CSS tab
    if ( class_exists( 'acf' ) ) {

            //Overlay newsletter background image
            $modal_img = _get_field('fabio_modal_img','option');
            $modal_img_css = '';
            if ( _get_field('enable_modal','option') && $modal_img ) $modal_img_css = ".fabio-modal-mailchimp .fabio-modal-content { background: url('.esc_url($modal_img).'); }";

        wp_add_inline_style( 'fabio-style', fabio_generate_dynamic_css() . $modal_img_css . _get_field('gg_css', 'option') );
    }

    wp_enqueue_script('fabio_translation_file', get_template_directory_uri() .'/js/lang.js');

    //Localize the script with new data
	$translation_array = array(
        'username_required'          => esc_html__( 'Username is required.', 'fabio' ),
        'email_required'             => esc_html__( 'Email is required.', 'fabio' ),
        'password_required'          => esc_html__( 'Password is required.', 'fabio' ),
        'error'                      => esc_html__( 'Error:','fabio'),
        'myaccount_page_url'         => ( fabio_is_wc_activated() ? get_permalink( wc_get_page_id('myaccount') ) : ''),
        'css_path'                   => trailingslashit( get_template_directory_uri() ),
        'ajax_url'                   => admin_url( 'admin-ajax.php' ),
        'pick_a_image'               => esc_html__('Pick a image:','fabio'),
        'you_can_select_just_1image' => esc_html__('You can select just a single image','fabio'),
        'noposts'                    => esc_html__('No older posts found', 'fabio'),
        'loadmore'                   => esc_html__('Load more posts', 'fabio'),
        'loading'                    => esc_html__('Loading', 'fabio')
	);
	wp_localize_script( 'fabio_translation_file', '__fabio__', $translation_array );

}

add_action('wp_enqueue_scripts', 'fabio_styles_loader');

function fabio_get_modal_background(){
    $img = _get_field('fabio_modal_img','option');

    if(empty($img)) return '';

    return ' style="background-image: url('.esc_url($img).')" ';
}

/**
 * Load JavaScript and jQuery files for theme.
 *
 */
function fabio_scripts_loader() {

    $setBase = (is_ssl()) ? "https://" : "http://";

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('fabio-comment-reply');
    }

    // Internet Explorer HTML5 support
    wp_enqueue_script( 'fabio-html5shiv',get_template_directory_uri().'/js/html5.js', array(), '', false);
    wp_script_add_data( 'fabio-html5shiv', 'conditional', 'lt IE 9' );

    /*Site plugins*/
    wp_enqueue_script('fabio-pack', get_template_directory_uri() . '/js/pack.js', array('jquery'), FABIO_THEMEVERSION, true);

    /* General */
    wp_enqueue_script('fabio-custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), FABIO_THEMEVERSION, true);

    /* Custom scripts from theme options Custom Scripts tab */
    $custom_scripts ='';
    if (_get_field('gg_script', 'option') != '') :
        $custom_scripts = stripslashes(_get_field('gg_script', 'option'));
    endif;
    wp_add_inline_script( 'fabio-custom', $custom_scripts );

    //Ajax
    wp_localize_script('fabio-custom', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}
add_action('wp_enqueue_scripts', 'fabio_scripts_loader');

/**
 * Admin js enqueue
 */
function fabio_jsenqueque(){
    wp_enqueue_script('fabio-admin-js', get_template_directory_uri() .'/js/admin.js');
    wp_enqueue_script('fabio_admin_translation_file', get_template_directory_uri() .'/js/admin-translations.js', array('jquery'));
    // Localize the script with new data
    $translation_array = array(
        'select_a_file' => esc_html__( 'Select a file', 'fabio' ),
        'gallery'       => esc_html__( 'Gallery', 'fabio' ),
        'insert'        => esc_html__( 'Insert','fabio'),
        'save_complete' => esc_html__( 'Save Complete!','fabio'),
    );
    wp_localize_script( 'fabio_admin_translation_file', '__fabio__', $translation_array );

    // Enqueued script with localized data.
    wp_enqueue_script( 'fabio_admin_translation_file' );
}
add_action( 'admin_enqueue_scripts', 'fabio_jsenqueque' );

/**
 * Display page header
 *
 */
if ( ! function_exists( 'fabio_page_header' ) ) :

function fabio_page_header() {

    //Get global post id
    $post_id            = fabio_global_page_id();

    $page_header       = _get_field('gg_page_header',$post_id, true);
    $page_header_align = _get_field('gg_page_header_align',$post_id, 'center');
    $page_title        = _get_field('gg_page_title',$post_id,true);
    $page_title_color  = _get_field('gg_page_title_color',$post_id,'');
    $page_subtitle     = _get_field('gg_page_subtitle',$post_id,'');
    $page_breadcrumbs  = _get_field('gg_page_breadcrumbs',$post_id,false);

    $blog_post_tags    = _get_field('gg_post_tags',$post_id,true);

    $page_description  = _get_field('gg_page_description',$post_id,'');
    //Get product category description
    if ( is_tax( array( 'product_cat', 'product_tag' ) ) && 0 === absint( get_query_var( 'paged' ) ) ) {
        $page_description = wc_format_content( term_description() );
    }

    $page_header_slider = _get_field('gg_page_header_slider',$post_id, false);
    $rev_slider_alias   = _get_field('gg_page_header_slider_select',$post_id);

    $page_header_img_style = $page_title_color_hex = '';
    if ( $page_title_color ) {
        $page_title_color_hex = 'style="color: '.$page_title_color.';"';
    }
    ?>

    <!-- Page header image -->
    <?php
        if ( has_post_thumbnail($post_id) && !is_singular('product') && !is_single() && !is_archive() && !is_search() && !is_post_type_archive() ) {
            $page_header_img_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' );
            $page_header_img_style = 'style="background-image: url('.$page_header_img_url[0].');"';
        }

        //Woocommerce category image
        if ( is_tax( 'product_cat') ) {
            global $wp_query;
            $cat = $wp_query->get_queried_object();
            $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
            $image = wp_get_attachment_url( $thumbnail_id );

            if ( $image ) {
                $page_header_img_url = $image;
                $page_header_img_style = 'style="background-image: url('.$page_header_img_url.');"';
            }
        }

        if ( is_post_type_archive( 'product' ) ) {
            $page_header_img_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' );
            $page_header_img_style = 'style="background-image: url('.$page_header_img_url[0].');"';
        }
    ?>
    <!-- End Page header image -->


    <?php if ($page_header_slider && function_exists('set_revslider_as_theme') ) : ?>
    <div class="subheader-slider">
        <div class="container">
            <?php putRevSlider(esc_html($rev_slider_alias)); ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php endif; ?>

    <?php
    if (
        ($page_header === TRUE || $page_header === NULL) &&
        !is_front_page() &&
        !is_404()
    ) :
    ?>
        <!-- Page meta -->
        <div class="page-meta">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-product-header">
                          <h1 <?php echo $page_title_color_hex; ?> ><?php echo fabio_wp_title(); ?></h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php if( $cat->description ) { ?>
                      <div class="col-md-8">
                        <div class="category-text">
                            <?php echo $cat->description?>
                        </div>
                      </div>
                    <?php } ?>
                    <?php if( $page_header_img_url ) { ?>
                      <div class="col-md-4">
                          <img src="<?php echo $page_header_img_url?>">
                          <?php /*
                          <div class="page-meta-wrapper">

                          <?php if ( ($page_title === TRUE OR $page_title === NULL) && !is_singular('product') )  : ?>

                          <?php if(is_singular('post')){ ?>

                              <div class="fabio-single-header">
      							<?php
                                      $previous   = get_previous_post_link( '<div class="single_left_header_pagination">%link</div>','<span class="fa fa-long-arrow-left"></span>' );
      							    $next       = get_next_post_link( '<div class="single_right_header_pagination">%link</div>', '<span class="fa fa-long-arrow-right"></span>' );
      							    echo wp_kses_post($previous);
                                  ?>
      							<h1 <?php echo $page_title_color_hex; ?>><?php echo fabio_wp_title(); ?></h1>
      							<?php echo wp_kses_post($next); ?>
  							</div>

  							<div class="post-date">
  								<time class="updated" datetime="<?php the_time( 'c' ) ; ?>"><?php echo get_the_date(); ?></time>
  							</div>

                              <?php if ( $blog_post_tags ) : ?>
                              <div class="posted-in-tags">
                                  <?php echo fabio_posted_in(); ?>
                              </div>
                              <?php endif; ?>

  							<?php } else if( is_singular('gallery_cpt') ) { ?>
  								<div class="fabio-gallery-single-header">
  									<h1><?php echo fabio_wp_title(); ?></h1>
  									<div class="toRight"><a href="<?php echo esc_url('/gallery-items/'); ?>"><?php esc_html_e('View All','fabio'); ?></a></div>
  								</div>

  							<?php }else { ?>
  								    <h1 <?php echo $page_title_color_hex; ?> ><?php echo fabio_wp_title(); ?></h1>
  							<?php } ?>
                          <?php endif; ?>

                          <?php
                          if ( $page_breadcrumbs === TRUE OR $page_breadcrumbs === NULL ) :
                              if (function_exists('fabio_breadcrumbs')) fabio_breadcrumbs();
                          endif;
                          ?>

                          <?php if ($page_description != '' ) : ?>
                          <div class="header-page-description">
                              <?php echo wp_kses_post($page_description); ?>
                          </div>
                          <?php endif; ?>
                          </div><!-- .page-meta-wrapper -->
                        */?>

                      </div><!-- .col-md-12 -->
                    <?php } ?>
                </div><!-- .row -->
            </div><!-- .container -->

        </div><!-- .page-meta -->
        <!-- End Page meta -->

    <?php endif; ?>

<?php
}
endif;

if ( ! function_exists( 'fabio_categories' ) ) :
    function fabio_categories() {

        if ( is_single() ) {
            $categories_list = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'fabio' ) );
            if ( $categories_list ) {
              printf( '<span class="cat-links"><span> %1$s </span>%2$s</span>',
                _x( 'in', 'Used before category names.', 'fabio' ),
                $categories_list
              );
            }

        }
    }
endif;

/**
 * Display template for post footer information (in single.php).
 *
 */
if (!function_exists('fabio_posted_in')) :
    function fabio_posted_in() {

    // Translators: used between list items, there is a space after the comma.
    $tag_list = get_the_tag_list('<ul class="list-inline post-tags"><li>','</li><li>','</li></ul>');

    // Translators: 1 is the tags
    if ( $tag_list ) {
        $utility_text = esc_html__( '%1$s', 'fabio' );
    }

    printf($tag_list);

}
endif;

/**
 * Adds custom classes to the array of body classes.
 *
 */
if (!function_exists('fabio_body_classes')) :
    function fabio_body_classes($classes) {

        $page_header_align           = _get_field('gg_page_header_align',fabio_global_page_id(), 'center');
        $page_header_slider          = _get_field('gg_page_header_slider', fabio_global_page_id(), false);
        $page_header_slider_position = _get_field('gg_page_header_slider_position', fabio_global_page_id(),'under_header');

        $footer = _get_field('gg_page_footer', fabio_global_page_id(),true);
        $footer_position = _get_field('gg_page_footer_position', fabio_global_page_id(),'opened');


        if ( has_post_thumbnail( fabio_global_page_id() ) && !is_singular('product') && !is_single() && !is_archive() && !is_search() && !is_post_type_archive() ) {
            $classes[] = 'gg-page-has-header-image';
        }
        //Woocommerce category image
        if ( is_tax( 'product_cat') ) {
            global $wp_query;
            $cat = $wp_query->get_queried_object();
            $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
            $image = wp_get_attachment_url( $thumbnail_id );

            if ( $image ) {
                $classes[] = 'gg-page-has-header-image';
            }
        }
        if ( $page_header_slider ) {
            $classes[] = 'gg-page-has-header-slider';
        }
        if ( $page_header_slider_position ) {
            $classes[] = 'gg-slider-is-'.$page_header_slider_position.'';
        }

        if ( $page_header_align ) {
            $classes[] = 'gg-page-header-align-'.$page_header_align.'';
        }

        //Header styles
        $nav_sticky = _get_field('gg_sticky_menu','option', false);
        $nav_menu = _get_field('gg_menu_style','option', 'style_1');

        if ($nav_sticky) {
            $classes[] = 'gg-has-stiky-menu';
        }

         if ($nav_menu) {
            $classes[] = 'gg-has-'.$nav_menu.'-menu';
        }
        //End header styles

        if (!is_multi_author()) {
            $classes[] = 'single-author';
        }

        if (!_get_field('gg_site_preloader', 'option',true)) {
            $classes[] = 'pace-not-active';
        }

        if ( fabio_is_wc_activated() && is_singular('product') ) {

            global $post;
            $product = wc_get_product ( $post->ID );

            $gg_product_thumbnails = $product->get_gallery_image_ids();
            if ( $gg_product_thumbnails ) {
                $classes[] = 'gg-product-has-thumbs';
            }
        }

        //WPML
        if ( fabio_is_wpml_activated() ) {

            $classes[] = 'gg-theme-has-wpml';

            //WPML currency
            if ( class_exists('woocommerce_wpml') ) {
                $classes[] = 'gg-theme-has-wpml-currency';
            }
        }

        //Shop columns
        $shop_cols = _get_field('gg_shop_product_columns', 'option', '3');
        if ( isset( $_GET['shop_cols'] ) ) {
            $shop_cols = $_GET['shop_cols'];
        }
        if ( $shop_cols ) {
            $classes[] = 'gg-shop-cols-'.$shop_cols;
        }

        //Shop style
        $shop_style = _get_field('gg_shop_style','option', 'style_1');
        if ( isset( $_GET['shop_style'] ) ) {
            $shop_style = $_GET['shop_style'];
        }
        if ( $shop_style ) {
            $classes[] = 'gg-shop-'.$shop_style;
        }

        //Footer style
        if ( $footer ) {
            $classes[] = 'footer-is-available';
        }

        if ( $footer && $footer_position ) {
            $classes[] = 'footer-is-' . $footer_position;
        }

        if ( class_exists( 'WooCommerce_Germanized' ) ) {
            $classes[] = 'gg-wc-germanized';
        }

        return $classes;
    }
    add_filter('body_class', 'fabio_body_classes');
endif;

/**
 * Footer extras module
 */
if (!function_exists('fabio_footer_extras')) :
    function fabio_footer_extras() { ?>
       	<div class="footer-extras">
			<?php if( _get_field('gg_footer_extras', 'option', true) ) : ?>
			<div class="container">
				<div class="row">
					<!-- Copyright -->
					<?php if ( _get_field('gg_footer_extras_copyright', 'option', true) != '') : ?>
					<div class="footer-copyright">
						<?php echo wp_kses_post( _get_field('gg_footer_extras_copyright', 'option', '&copy; '.date('Y').' Honayda. All rights reserved') ); ?>
					</div>
					<?php endif; ?>
				</div><!-- /footer-extras -->
			</div>
		</div>
        <?php endif; ?>

    <?php }
endif;


/**
 * Replaces the login header logo
 */
if (!function_exists('fabio_admin_login_style')) :
    add_action( 'login_head', 'fabio_admin_login_style' );
    function fabio_admin_login_style() {
        if ( _get_field('gg_display_admin_image_logo', 'option') ) {
            $logo = _get_field('gg_admin_logo_image', 'option');
        ?>
            <style>
            .login h1 a {
                background-image: url( <?php echo esc_url($logo['url']); ?> ) !important;
                background-size: <?php echo esc_attr($logo['width']); ?>px <?php echo esc_attr($logo['height']);?>px;
                width:<?php echo esc_attr($logo['width']); ?>px;
                height:<?php echo esc_attr($logo['height']); ?>px;
                margin-bottom:15px;
            }
            </style>
        <?php
        }
    }
endif;

/* Modify the titles */
add_filter( 'get_the_archive_title', function ( $title ) {

    if( is_category() ) {
        $title = single_cat_title( '', false );
    }

    if ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    }

    return $title;

});

function fabio_footer_logo(){
	  // Displays H1 or DIV based on whether we are on the home page or not (SEO)
        $heading_tag = ( is_home() || is_front_page() ) ? 'h2' : 'div';
      if ( _get_field('gg_display_image_logo', 'option', false) ) {

	        //Theme Logo
            $default_logo = array(
                'url' => get_template_directory_uri() . '/images/logo.png',
                'width' => '230',
                'height' => '64',
            );
		 $logo = _get_field('gg_logo_image', 'option', $default_logo);


            //If logo is not yet imported display default logo
            if ($logo == false)
                $logo = $default_logo;

            echo '<a class="footer-brand" href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo('name','display')).'" rel="home">'. "\n";
            echo '<img class="footer-brand" src="'.esc_url($logo['url']).'" alt="'.esc_attr( get_bloginfo('name','display')).'" />'. "\n";
            echo '</a>'. "\n";

	} else {
        $class="text site-title";
        echo '<'.$heading_tag.' class="'.esc_attr($class).'">';
        echo '<a href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo('name','display')).'" rel="home">'.get_bloginfo('name').'</a>';
        if ( _get_field('gg_display_tagline', 'option', false) ) {
            echo '<small class="visible-desktop visible-tablet '.esc_attr($class).'">'.get_bloginfo('description').'</small>'. "\n";
        }
        echo '</'.$heading_tag.'>'. "\n";
    }
}

/**
 * Theme logo
 */
if (!function_exists('fabio_logo')) :
    function fabio_logo() {
        // Displays H1 or DIV based on whether we are on the home page or not (SEO)
        $heading_tag = ( is_home() || is_front_page() ) ? 'h2' : 'div';

        if ( _get_field('gg_display_image_logo', 'option', false) ) {

            $class="graphic";
            $margins_style = '';

            //Theme Logo
            $default_logo = array(
                'url' => get_template_directory_uri() . '/images/logo.png',
                'width' => '230',
                'height' => '64',
            );

            $logo = _get_field('gg_logo_image', 'option', $default_logo);


            //If logo is not yet imported display default logo
            if ($logo == false)
                $logo = $default_logo;


            $margins = _get_field('gg_logo_margins', 'option');

            if ($margins) {
                $margins_style = 'style="margin: '.esc_attr($margins).';"';
            }

            echo '<a class="brand" href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo('name','display')).'" rel="home">'. "\n";
            echo '<img '.$margins_style.' class="brand" src="'.esc_url($logo['url']).'" alt="'.esc_attr( get_bloginfo('name','display')).'" />'. "\n";
            echo '</a>'. "\n";
        } else {
            $class="text site-title";
            echo '<'.$heading_tag.' class="'.esc_attr($class).'">';
            echo '<a href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo('name','display')).'" rel="home">'.get_bloginfo('name').'</a>';
            if ( _get_field('gg_display_tagline', 'option', false) ) {
                echo '<small class="visible-desktop visible-tablet '.esc_attr($class).'">'.get_bloginfo('description').'</small>'. "\n";
            }
            echo '</'.$heading_tag.'>'. "\n";
        }
    }
endif;


/**
 * Admin notice for WPML and Polylang together
 */
function fabio_admin_notices() {
	if(has_action('wpml_add_language_selector') && (defined('POLYLANG_VERSION'))){
	   ?>
		<div class="notice notice-warning is-dismissible">
			<p><?php esc_html_e( 'You have Polylang and WPML enabled! Polylang will be used!', 'fabio' ); ?></p>
		</div>
		<?php
	}
}
add_action( 'admin_notices', 'fabio_admin_notices' );


/**
 * Move comment field to bottom
 */
function fabio_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}
add_filter( 'comment_form_fields', 'fabio_move_comment_field_to_bottom' );

/**
 * Query for search page to display more the 1 result
 */

add_filter('posts_orderby', 'fabio_group_by_post_type', 10, 2);
function fabio_group_by_post_type($orderby, $query) {
    global $wpdb;
    if ($query->is_search) {
        return $wpdb->posts . '.post_type DESC';
    }
    // provide a default fallback return if the above condition is not true
    return $orderby;
}

function fabio_modal2footer() {
    //My Account modal
    if ( fabio_is_wc_activated() ) {
        get_template_part("parts/part","modal");
    }
	//Mailchimp modal
	if( _get_field('enable_modal','option') ) {
		get_template_part("parts/part","modal-mailchimp");
	}
}
add_action( 'wp_footer', 'fabio_modal2footer', 9999 );

if ( !function_exists('fabio_ajax_posts_link') ) :
    function fabio_ajax_posts_link ($label, $max_num_pages,$pageId){
    	$html = '';

    	$html .='<a href="#" class="btn btn-default" ';
    	$html .= 'id="fabio-archive-load-more" data-curpage="2" ';
    	$html .= 'data-pagecount="'.$max_num_pages.'" ';
    	$html .= 'data-pageid="'.$pageId.'" ';
    	$html .= 'data-category="'.get_query_var('cat').'" ';
    	$html .= '>'.$label.'</a>';
    	return $html;
    }
endif;

function fabio_get_arhive_ajax_items_callback(){

    $category  = false;
    $curpage   = 1;
    $pageCount = 0;
    $pageid    = 0;

	if(isset($_POST['pageid']) && $_POST['pageid'] > 0) $pageid = (int)$_POST['pageid'];
	if(isset($_POST['curpage']) && $_POST['curpage'] > 0) $curpage = (int)$_POST['curpage'];
	if(isset($_POST['category']) && $_POST['category'] > 0 ) $category = (int)$_POST['category'];

    $blog_no_posts = _get_field('gg_blog_no_of_posts_to_show',$pageid,5);
    $blog_columns  = _get_field('gg_blog_columns',$pageid,1);

	$args = array(
        'suppress_filters' => true,
        'post_type'        => 'post',
        'posts_per_page'   => $blog_no_posts,
        'paged'            => $curpage,
    );

    $loop = new WP_Query($args);

    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();?>
	<li class="isotop-grid-item isotope-item post-item col-xs-12 col-md-<?php echo esc_attr(floor( 12 / $blog_columns )); ?>">
		<?php get_template_part( 'parts/post-formats/part', get_post_format() ); echo get_post_format() ;?>
	</li><?php

    endwhile;
    endif;
    wp_reset_postdata();

	wp_die();
}
add_action( 'wp_ajax_fabio_get_arhive_ajax_items', 'fabio_get_arhive_ajax_items_callback' );
add_action( 'wp_ajax_nopriv_fabio_get_arhive_ajax_items', 'fabio_get_arhive_ajax_items_callback' );


if (!function_exists('fabio_more_post_ajax')) {
    function fabio_more_post_ajax(){

        $ppp     = (isset($_POST['ppp'])) ? $_POST['ppp'] : 3;
        $cat     = (isset($_POST['cat'])) ? $_POST['cat'] : 0;
        $offset  = (isset($_POST['offset'])) ? $_POST['offset'] : 0;
        if(isset($_POST['pageid']) && $_POST['pageid'] > 0) $pageid = (int)$_POST['pageid'];

        $blog_no_posts = _get_field('gg_blog_no_of_posts_to_show',$pageid,5);
        $blog_columns  = _get_field('gg_blog_columns',$pageid,1);


        $args = array(
            'post_type'      => 'post',
            'posts_per_page' => $ppp,
            'cat'            => $cat,
            'offset'         => $offset,
        );

        $loop = new WP_Query($args);

        if ($loop -> have_posts()) :
            while ($loop -> have_posts()) :
                $loop -> the_post();
                ?>
                <li class="isotop-grid-item isotope-item post-item col-xs-12 col-md-<?php echo esc_attr(floor( 12 / $blog_columns )); ?>">
                    <?php get_template_part( 'parts/post-formats/part', get_post_format() ); echo get_post_format() ;?>
                </li>
                <?php
            endwhile;
        endif;

        wp_reset_postdata();

        wp_die();
    }
}
add_action('wp_ajax_nopriv_fabio_more_post_ajax', 'fabio_more_post_ajax');
add_action('wp_ajax_fabio_more_post_ajax', 'fabio_more_post_ajax');

/**
 * Disable Brizy notification */
function hide_update_noticee_to_all_but_admin_users() {
    set_transient( 'fw_brz_admin_notice', 1 );
}
add_action( 'admin_head', 'hide_update_noticee_to_all_but_admin_users', 1 );
