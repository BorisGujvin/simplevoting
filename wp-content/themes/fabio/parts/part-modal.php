<div class="fabio-modal" id="fabio-modal-login" role="dialog" >
<div class="fabio-modal-wrapper">
	
	<div class="fabio-modal-content <?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) {echo 'withRegisterForm';  }  ?>">
		<div class="close">
            <a href="#"><?php esc_html_e('Close','fabio'); ?></a>
	   </div>
	<?php 
    if( !is_user_logged_in() ) {
        echo do_shortcode('[woocommerce_my_account]');
    } else {
    ?>
        <?php $user = wp_get_current_user(); ?>
	    <h2><?php esc_html_e('Welcome!','fabio');?></h2>
        
        <p><?php printf( __( 'Logged in as <strong>%s</strong>', 'woocommerce' ), esc_html( $user->display_name ) ); ?></p>
        <p>
            <?php esc_html_e('Go to ','fabio');?>
            <a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
            <?php esc_html_e('or ','fabio');?>
            <a href="<?php echo esc_url( wp_logout_url( get_permalink() ) ); ?>" class="wc-auth-logout"><?php _e( 'Logout', 'woocommerce' ); ?></a>
        </p>
	
    <?php } ?>
	</div>
	
</div>
</div>	