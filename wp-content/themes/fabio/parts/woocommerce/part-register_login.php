<div class="whiteModal" id="woo-login-register" role="dialog" >
<div class="white-modal-item">
	<div class="close">
		<a href="#" class="fa fa-remove"></a>
	</div>
	
	<div class="wooform">
		<div class="woowrapper" id="cwoocommerce-custom-login">
            <div class="error"></div>
			<?php if( is_user_logged_in() ){ ?>
				<?php esc_html_e('You are already logged In','fabio'); ?>
                <p><?php printf( __( 'Logged in as %s', 'woocommerce' ), esc_html( $user->display_name ) ); ?> <a href="<?php echo esc_url( $logout_url ); ?>" class="wc-auth-logout"><?php _e( 'Logout', 'woocommerce' ); ?></a>
			<?php }else{  ?>

            <?php get_template_part( 'parts/woocommerce/part', 'ajax-form-login' ); ?>
		
			<?php  } ?>
		</div>
	</div>

</div>
</div>	