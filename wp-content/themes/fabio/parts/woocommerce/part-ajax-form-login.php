<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

<div class="u-columns col2-set" id="customer_login">

	<div class="u-column1 col-1">

<?php endif; ?>

		<h2><?php _e( 'login', 'fabio' ); ?></h2>

		<form method="post" class="login" id="fabio_ajax_login">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
			
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" placeholder="<?php esc_html_e( 'Username or email address *', 'fabio' ); ?>" name="username_ajax" id="username_ajax" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
			</p>
			<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
				<input class="woocommerce-Input woocommerce-Input--text input-text" placeholder="<?php esc_html_e( 'Password', 'fabio' ); ?>" type="password" name="password_ajax" id="password_ajax" />
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<div class="form-row">
				<?php wp_nonce_field( 'woocommerce_login_ajax', 'woocommerce_login_ajax' ); ?>
				<div class="forgot-remember">
					<label for="rememberme_ajax" class="inline">
						<input class="woocommerce-Input woocommerce-Input--checkbox rememberme" name="rememberme_ajax" type="checkbox" id="rememberme_ajax" value="forever" /> <?php _e( 'Remember me', 'fabio' ); ?>
					</label>
					<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="lost-password"><?php _e( 'Lost your password?', 'fabio' ); ?></a>
				</div>
				<div class="login-button">
				<input type="submit" class="woocommerce-Button button" name="login_ajax" id="login_ajax" value="<?php esc_attr_e( 'Login', 'fabio' ); ?>" />
				</div>
			</div>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

	</div>

	<div class="u-column2 col-2">

		<h2><?php _e( 'register', 'fabio' ); ?></h2>

		<form method="post" class="register" id="fabio_ajax_register_form">

			<?php do_action( 'woocommerce_register_form_start' ); ?>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
				
					<input type="text" placeholder="<?php esc_html_e( 'Username', 'fabio' ); ?>" class="woocommerce-Input woocommerce-Input--text input-text" name="username_ajax_register" id="reg_username_ajax_register" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username_ajax_register'] ); ?>" />
				</p>

			<?php endif; ?>

			<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
				<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" placeholder="<?php esc_html_e( 'Email address', 'fabio' ); ?> " name="email_ajax_register" id="reg_email_ajax_register" value="<?php if ( ! empty( $_POST['email_ajax_register'] ) ) echo esc_attr( $_POST['email_ajax_register'] ); ?>" />
			</p>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

				<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
					
					<input placeholder="<?php esc_html_e( 'Password', 'fabio' ); ?>" type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password_ajax_register" id="reg_password_ajax_register" />
				</p>

			<?php endif; ?>

			<!-- Spam Trap -->
			<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'fabio' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" autocomplete="off" /></div>

			<?php do_action( 'woocommerce_register_form' ); ?>
			<?php do_action( 'register_form' ); ?>

			<p class="woocomerce-FormRow form-row woo-register">
				<?php wp_nonce_field( 'register_nonce', 'register_nonce' ); ?>
				<input type="submit" class="woocommerce-Button button" id="register_ajax" name="register" value="<?php esc_attr_e( 'Register', 'fabio' ); ?>" />
			</p>

			<?php do_action( 'woocommerce_register_form_end' ); ?>

		</form>

	</div>

</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
