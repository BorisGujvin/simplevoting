<nav class="st-menu st-effect-1" id="menu-1">
<div class="fabio-woo-mini-cart">
	<?php
	    if( fabio_is_wc_activated() ) : 
        //WC minicart
        if( _get_field('gg_header_minicart', 'option', true) ) {
            get_template_part( 'parts/part', 'wc-minicart' ); 
        }
        //WC login/register
        if( _get_field('gg_header_account', 'option', true) ) {
         //   get_template_part( 'parts/woocommerce/part', 'register_login' );
        }
    endif;
	
	?>
</div>
</nav>