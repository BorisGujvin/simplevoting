<div class="product-image">
    <?php the_post_thumbnail( 'medium' );?>
</div>

<div class="product-body">
	<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
</div>