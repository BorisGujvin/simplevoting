<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage fabio
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php fabio_post_thumbnail(); ?>

    <?php if ( !is_single() ) : ?>
    	<div class="fabio-datetime">
    	<?php
    		if ( the_title( ' ', ' ', false ) == "" ) {
    			echo '<time class="updated" datetime="'. get_the_time( 'c' ) .'">'. sprintf( '<a href="%1$s" rel="bookmark"> %2$s </a>', get_permalink(), get_the_date('F dS, Y') ) .'</time>';
    		} else {
    			echo '<time class="updated" datetime="'. get_the_time( 'c' ) .'">'.get_the_date('F dS, Y')  .'</time> ';
    		}
    	?>
    	</div>
        <header class="entry-header">
        	<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
        </header><!-- .entry-header -->

    <div class="collection-carousel-wrapper">
    <?php 	
        $uid = 'collection_'.get_the_id();
        $primary_img = get_field('primary_image'); 
        $secondary_image = get_field('secondary_image'); 
    ?>
    	<div class="collection-carousel-slick" id="carusel-<?php echo esc_attr($uid); ?>" data-uid="<?php echo get_the_id(); ?>" >
    		
    		<div class="one-collection">
    			<h3><?php echo get_the_title(); ?></h3>
    			<img src="<?php echo esc_url($primary_img['sizes']['medium_large']); ?>" alt="<?php echo esc_attr($primary_img['title']);?>" class="collection-primary">
    			<img src="<?php echo esc_url($secondary_image['sizes']['medium_large']); ?>" alt="<?php echo esc_attr($secondary_image['title']);?>" class="collection-secondary">
    		</div>

    	</div>

    	<div class="collection-carousel-pagination" id="collection-carousel-pagination">
    	
        <a href="#" class="fa fa-angle-left prev" id="carusel2-prev-<?php echo esc_attr($uid);?>"></a>
    	<a href="<?php the_permalink();?>" class="view-button"><?php  esc_html_e('View Collection','fabio'); ?></a>
    	<a href="#" id="carusel2-next-<?php echo esc_attr($uid);?>" class="fa fa-angle-right next"></a></div>
    		
    	<div id="carusel-<?php echo esc_attr($uid); ?>pagInfo" class="collection-carousel-pagInfo">1/1</div>
    	
    </div>
    <?php endif; ?>

    <?php if ( is_single() ) : ?>
    <div class="entry-content">
        <?php
        	/* translators: %s: Name of current post */
        	the_content( sprintf(
        		esc_html__( 'Continue reading %s', 'fabio' ),
        		the_title( '<span class="screen-reader-text">', '</span>', false )
        	) );

        	wp_link_pages( array(
        		'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'fabio' ) . '</span>',
        		'after'       => '</div>',
        		'link_before' => '<span>',
        		'link_after'  => '</span>',
        		'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'fabio' ) . ' </span>%',
        		'separator'   => '<span class="screen-reader-text">, </span>',
        	) );
        ?>
    </div><!-- .entry-content -->
    <?php endif; ?>

</article><!-- article -->
