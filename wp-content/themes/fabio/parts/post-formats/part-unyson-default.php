<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage fabio
 */
?>

<?php global $fabio_enabled_thumb, $fabio_enabled_date; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


<?php
if ( $fabio_enabled_thumb ) { 
    fabio_post_thumbnail(); 
    $fabio_withoutImage_class = '';
} else {
    $fabio_withoutImage_class = 'withoutimage';
}
?>

<header class="entry-header">
    <?php the_title( sprintf( '<h2 class="entry-title '.$fabio_withoutImage_class .'"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a><span class="blogpost-type"></span></h2>' ); ?>
</header><!-- .entry-header -->

<div class="unyson-post-excerpt">

    <?php if ( $fabio_enabled_date ) : ?>
    <div class="fabio-datetime">
        <span class="fa fa-clock-o"></span>
        <?php echo '<time class="updated" datetime="'. get_the_time( 'c' ) .'">'. get_the_date() .'</time>'; ?>
    </div>
    <?php endif; ?>
    
    <p>
        <a href="<?php the_permalink(); ?>">
            <?php
                $text = wp_strip_all_tags( get_the_excerpt(), true ); 
        	   echo substr($text,0,140).'[...] <span>'. esc_html__('CONTINUE READING','fabio').'</span>';
            ?>
        </a>
    </p>
</div>
</article>