<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage fabio
 */
?>

<?php
$blog_share_box  = _get_field('gg_post_social_share','',true);
$blog_categories = _get_field('gg_post_categories','',true);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<?php fabio_post_thumbnail(); ?>

		<?php if ( !is_single() ) : ?>
		
        <div class="fabio-datetime">
            <?php
            if ( the_title( ' ', ' ', false ) == "" ) {
                echo '<time class="updated" datetime="'. get_the_time( 'c' ) .'">'. sprintf( '<a href="%1$s" rel="bookmark"> %2$s </a>', get_permalink(), get_the_date('F dS, Y') ) .'</time>';
            } else {
                echo '<time class="updated" datetime="'. get_the_time( 'c' ) .'">'.get_the_date('F dS, Y')  .'</time> ';
            }
            ?>
        </div>

		<header class="entry-header">
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		</header><!-- .entry-header -->

		
		<?php endif; ?>

		<?php if ( is_search() || has_excerpt() && ! is_single() ) : ?>

		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
		
		<?php else:  ?>

		<div class="entry-content">
			<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				esc_html__( 'Continue reading %s', 'fabio' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'fabio' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'fabio' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
		</div><!-- .entry-content -->
		<?php endif; ?>

		<?php if ( is_single() ) : ?>

    		<footer class="entry-meta">
                
        		<?php if ($blog_share_box) : ?>	
        	    <div class="btn-group btn-group-justified pagination-wrapper">
                    <div class="btn-group" role="group">
        				<div class="post-social">
        					<?php do_action( 'fabio_social_share' ); ?>
        				</div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if ($blog_categories) : ?>
                <div class="posted-in-categories">
                    <?php echo fabio_categories(); ?>
                </div>
                <?php endif; ?>

    		</footer><!-- .entry-meta -->


		<?php endif; ?>


</article><!-- article -->
