<div class="fabio-modal fabio-modal-mailchimp" id="fabio-modal-mailchimp" role="dialog" data-delay="<?php echo _get_field('fabio-modal-delay','option') ; ?>"  >
    <div class="fabio-modal-wrapper">

    	<div class="fabio-modal-content" <?php echo fabio_get_modal_background(); ?>>
            <div class="close"><a href="#" class=""><?php esc_html_e('Close','fabio'); ?></a></div>
            <?php  echo do_shortcode('[mc4wp_form]');?>
    	</div>
    	
    </div>
</div>	