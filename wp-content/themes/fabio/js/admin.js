function FabioPickFile(id,type){
		jQuery(id).val('');
	if(type == 'gallery'){
		var frame = wp.media({  
			title: __fabio__.select_a_file ,
			multiple: true,
			button: {text: __fabio__.insert}});

		frame.on('close',function() {
			var selection = frame.state().get('selection');
		 
		});
		frame.on( 'select',function() {

		var state = frame.state();
		var selection = state.get('selection');
		if ( ! selection ) return;
		var items = new Array();
		selection.each(function(attachment) {
				var id  = parseInt(attachment.attributes.id);
				if(!isNaN(id)) items.push(id);
			

		});
		jQuery(id).val( items.join(','));
	});

	frame.open();

	}else{	
		var frame = wp.media({  
			title: __fabio__.select_a_file ,
			library: {type: type},
			multiple: false,
			button: {text: __fabio__.insert}});

		frame.on('close',function() {
			var selection = frame.state().get('selection');
		 
		});
		frame.on( 'select',function() {

		var state = frame.state();
		var selection = state.get('selection');
		if ( ! selection ) return;
		selection.each(function(attachment) {
			  var url = attachment.attributes.url;
			jQuery(id).val(attachment.attributes.url);

		});
	});

	frame.open();
	}
}

function ct_media_upload(button_class,remove_class) {
	jQuery(document).ready( function($) {
		$('body').on('click', button_class, function(e) {   	
			//get a reference to the current button
			var _this = this;
			//init wordpress media
			var custom_uploader = wp.media({
					title: __fabio__.pick_a_image ,
					button: {
						text: __fabio__.you_can_select_just_1image
					},
					multiple: false  // Set this to true to allow multiple files to be selected
			})// when a file is selected
			.on('select', function() {
					var attachment = custom_uploader.state().get('selection').first().toJSON();
					$($(_this).data('target')).val(attachment.id);
					var wrapperid =$(_this).data('wrapper');
					$(wrapperid).html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
			   
				   if(attachment.mime == 'image/svg+xml') {
					   $(wrapperid+' .custom_media_image').attr('src',attachment.url).css('display','block').addClass('svg');   
				   }else{
						$(wrapperid+' .custom_media_image').attr('src',attachment.sizes.thumbnail.url).css('display','block').removeClass('svg');
				   }
				}).open();
		});	   
		//delete 
		$('body').on('click',remove_class,function(){

			$($(this).data('target')).val('');
			var wrapper = $(this).data('wrapper');
			$(wrapper).html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
			$(wrapper).html('');
		});		   
	});// end on document ready

}//end function
	 
jQuery(document).ready( function($) { 


	if($('.ct_tax_media_button1.button').length > 0) ct_media_upload('.ct_tax_media_button1.button','.ct_tax_media_remove1'); 

});// end on document ready

