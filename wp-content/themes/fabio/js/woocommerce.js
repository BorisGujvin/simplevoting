jQuery( function( $ ) {
	
if($('.navbar-secondary #secondary-menu li.menu-item-has-mega-menu').length > 0 ){	
	$('.navbar-secondary #secondary-menu li.menu-item-has-mega-menu').hoverIntent( function(){
	   $(this).addClass('megamenu-over');	
	}, function(){
        $(this).removeClass('megamenu-over');
	});	
}

$('.navbar-secondary #secondary-menu li.menu-item-has-mega-menu .mega-menu').hoverIntent( function(){
	$(this).parent().addClass('megamenu-over');
	
} , function(){
	$(this).parent().removeClass('megamenu-over');

	
})

    if( $('.woocommerce div.product div.images .flex-control-thumbs').length >  0 ) {
    	
    	$('.woocommerce div.product div.images .flex-control-thumbs').slick({
    		dots: false,
    		infinite: false,
    		speed: 300,
    		slidesToShow: 4,
    		slidesToScroll: 1,
    		vertical: true,
    		arrows: true,
    		swipe: false
    	});
    	
    }
	
	// Quantity buttons
	if ( ! String.prototype.getDecimals ) {
		String.prototype.getDecimals = function() {
			var num = this,
				match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
			if ( ! match ) {
				return 0;
			}
			return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
		}
	}

	function fabio_refresh_quantity_increments(){
		$( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input type="button" value="+" class="plus" />' ).prepend( '<input type="button" value="-" class="minus" />' );
	}

	$( document ).on( 'updated_wc_div', function() {
		fabio_refresh_quantity_increments();
	} );

	$( document ).on( 'click', '.plus, .minus', function() {
		// Get values
		var $qty		= $( this ).closest( '.quantity' ).find( '.qty'),
			currentVal	= parseFloat( $qty.val() ),
			max			= parseFloat( $qty.attr( 'max' ) ),
			min			= parseFloat( $qty.attr( 'min' ) ),
			step		= $qty.attr( 'step' );

		// Format values
		if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
		if ( max === '' || max === 'NaN' ) max = '';
		if ( min === '' || min === 'NaN' ) min = 0;
		if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

		// Change the value
		if ( $( this ).is( '.plus' ) ) {
			if ( max && ( currentVal >= max ) ) {
				$qty.val( max );
			} else {
				$qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
			}
		} else {
			if ( min && ( currentVal <= min ) ) {
				$qty.val( min );
			} else if ( currentVal > 0 ) {
				$qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
			}
		}

		// Trigger change event
		$qty.trigger( 'change' );
	});

	fabio_refresh_quantity_increments();
	
	//Scroll to shipping fields
	if($('#ship-to-different-address-checkbox').length > 0) {
		
		$("#ship-to-different-address-checkbox").change(function() {
			if(this.checked) {
			    $('html,body').animate({
			        scrollTop: $(".woocommerce-shipping-fields").offset().top},
			        'slow');
			}
		});
	}
	

	//Ajax login
	$('.woocommerce-Button[name="login"]').on('click', function(e) {
		
        var gg_data = {
			'action': 'fabio_login_user',
			'username': $('.woocommerce-Input[name="username"]').val(),
			'password': $('.woocommerce-Input#password').val(),
			'woocommerce-login-nonce':$('#woocommerce-login-nonce').val(),
			'rememberme':$('.woocommerce-form__input-checkbox[name="rememberme"]').is(":checked")?1:''
		};
	
		if(gg_data.username =='' || gg_data.password=='') {
            $('.fabio-login-modal .woocommerce-error').remove();
			var  error = '<ul class="woocommerce-error">';
				if(gg_data.username =='') error += '<li> <strong>'+__fabio__.error+' </strong>'+__fabio__.username_required+'</li>';
				if(gg_data.password =='') error += '<li> <strong>'+__fabio__.error+' </strong>'+__fabio__.password_required+'</li>';
			
			error += '</ul>';
			$('.fabio-login-modal').prepend(error);

		} else {
            $.ajax({
                type: 'POST',
                dataType: 'html',
                url: ajax_wc_object.ajax_url,
                data: gg_data,
                beforeSend : function () {
                   $('.woocommerce-Button[name="login"]').attr('disabled','disabled');
                   $('.fabio-login-modal').prepend('<div class="gg-ajax-loading"></div>');
                },
                success: function (gg_data) {

                    if (gg_data == 1) { 
                        window.location.href = ajax_wc_object.myaccount_page_url;
                    } else {
                        var error = '<ul class="woocommerce-error">';
                            error += '<li>'+gg_data+'</li>';
                            error += '</ul>';
                        
                        $('.fabio-login-modal .woocommerce-error').remove();
                        $('.fabio-login-modal').prepend(error);    

                        $('.woocommerce-Button[name="login"]').removeAttr('disabled');
                        $('.fabio-login-modal .gg-ajax-loading').remove();
                    }
                },
                error : function (jqXHR, textStatus, errorThrown) {
                    jQuery('.fabio-login-modal').html($.parseJSON(jqXHR.responseText) + ' :: ' + textStatus + ' :: ' + errorThrown);
                },
            });
		}
		e.preventDefault();
	
	});
	
	//Ajax register
    $('.woocommerce-Button[name="register"]').on('click', function(e) {
        
        var gg_data = {
            'action': 'fabio_register_user',
            'email': $('.fabio-register-modal .woocommerce-Input[name="email"]').val(),
            'username': $('.fabio-register-modal .woocommerce-Input[name="username"]').val(),
            'password': $('.fabio-register-modal .woocommerce-Input#reg_password').val(),
            'woocommerce-register-nonce':$('#woocommerce-register-nonce').val(),
        };
    
        if(gg_data.email =='' || gg_data.password=='') {
            $('.fabio-register-modal .woocommerce-error').remove();
            var  error = '<ul class="woocommerce-error">';
                if(gg_data.email =='') error += '<li> <strong>'+__fabio__.error+' </strong>'+__fabio__.username_required+'</li>';
                if(gg_data.password =='') error += '<li> <strong>'+__fabio__.error+' </strong>'+__fabio__.password_required+'</li>';
            
            error += '</ul>';
            $('.fabio-register-modal').prepend(error);

        } else {
            $.ajax({
                type: 'POST',
                dataType: 'html',
                url: ajax_wc_object.ajax_url,
                data: gg_data,
                beforeSend : function () {
                   $('.woocommerce-Button[name="register"]').attr('disabled','disabled');
                   $('.fabio-register-modal').prepend('<div class="gg-ajax-loading"></div>');
                },
                success: function (gg_data) {

                    if (gg_data == 1) { 
                        window.location.href = ajax_wc_object.myaccount_page_url;
                    } else {
                        var error = '<ul class="woocommerce-error">';
                            error += '<li>'+gg_data+'</li>';
                            error += '</ul>';
                        
                        $('.fabio-register-modal .woocommerce-error').remove();
                        $('.fabio-register-modal').prepend(error);    

                        $('.woocommerce-Button[name="register"]').removeAttr('disabled');
                        $('.fabio-register-modal .gg-ajax-loading').remove();
                    }
                },
                error : function (jqXHR, textStatus, errorThrown) {
                    jQuery('.fabio-register-modal').html($.parseJSON(jqXHR.responseText) + ' :: ' + textStatus + ' :: ' + errorThrown);
                },
            });
        }
        e.preventDefault();
    
    });
	
	if($('.login').length > 0 ){
		$('.login.menu-item').on('click touchend', function(e){
			$('#fabio-modal-login').fadeIn();
				e.preventDefault();
				return false;
		});
		$('.close').on('click touchend', function(e){
			$('#fabio-modal-login').fadeOut();
				e.preventDefault();
				return false;
		});	
	}
	
});