(function ($) {
"use strict";

function gg_isotope_init() {

	if($('.el-grid:not(.gg-slick-carousel)').length > 0){
	    jQuery('.masonry_post').each(function(){
	        var $grid = jQuery(this);
	        var layout_mode = $(this).data('layout-mode');

			var _this = this;

	        jQuery(window).on('load resize', function() {
				$grid.imagesLoaded( function() {
					$grid.isotope({
						itemSelector : '.isotop-grid-item',
						layoutMode : layout_mode//fitRows or masonry
					});	
					
				});
	        });

	    });
	}
}

/* Magnific */
function gg_magnific_init() {
	
	if($('.maginific-simple').length > 0) {
		
		$('.maginific-simple').magnificPopup({type:'image'});
	}
	if($('.el-grid:not(.no_magnific), .gg-slick-carousel.has_magnific, .wpb_image_grid.has_magnific, .wpb_single_image.has_magnific, .post-thumbnail.has_magnific, .size-guide-wrapper.has_magnific, .gg-contact-template').length > 0){
		$( '.el-grid:not(.no_magnific), .gg-slick-carousel.has_magnific, .wpb_image_grid.has_magnific, .wpb_single_image.has_magnific, .post-thumbnail.has_magnific, .size-guide-wrapper.has_magnific, .gg-contact-template' ).each(function(){
			$(this).magnificPopup({
				delegate: 'a.lightbox-el',
				type: 'image',
				gallery: {
		            enabled: true
		        },
				callbacks: {
				    elementParse: function(item) {
				    	if(item.el.context.className == 'lightbox-el link-wrapper lightbox-video') {
				        	item.type = 'iframe';
				    	} else if(item.el.context.className == 'lightbox-el gg-popup') {
				        	item.type = 'inline';
				        } else {
				        	item.type = 'image';
				      	}
				    }
				}
			});
		});
	}
}

/* SlickCarousel */
function gg_slickcarousel_init() {
	if($('.gg-slick-carousel:not(.gg_filter)').length > 0){
		$( '.gg-slick-carousel:not(.gg_filter)' ).each(function(){

			var $this = $(this);
			var filtered = false;

			$('.gg_filter.gg-slick-carousel a').on('click', function(e){
				e.preventDefault();
				$(this).parent().parent().find('.active').removeClass('active');
				$(this).parent().addClass('active');

		        var gg_filter = $(this).parent().parent().parent().parent().find('.el-grid.gg-slick-carousel');

		        if ($(this).attr('data-filter') == '*') {
			      	gg_filter.slick('slickUnfilter');
			      	gg_filter.slick('slickGoTo',0);
				    filtered = false;
			    } else {
				  	gg_filter.slick('slickFilter',$(this).attr('data-filter'));
				  	gg_filter.slick('slickGoTo',0);
				    filtered = true;
			    } 
			});

		});

	}
}

function fabio_update_slick_slider(selector){
	
    var id        = $(selector).data('uid');
    var with_dots = $(selector).data('dots');
    var rtl_mode  = $(selector).data('rtl');

	$(selector).slick({
        centerMode: false,
        dots: with_dots,
        prevArrow: $('#carusel-prev-'+id),
        nextArrow: $('#carusel-next-'+id),
        centerPadding: '60px',
        slidesToShow: $(selector).data('columns'),
        rtl: rtl_mode,
		responsive: [
			{
			  breakpoint: 992,
			  settings: {
				arrows: false,
				centerMode: false,
				centerPadding: '40px',
				slidesToShow: 3
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				arrows: false,
				centerMode: false,
				centerPadding: '40px',
				slidesToShow: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				arrows: false,
				centerMode: false,
				centerPadding: '40px',
				slidesToShow: 1
			  }
			}
		]
	});	
}


function fabio_carousel_slick_collection(selector){
	
    var id       = $(selector).data('uid');
    var rtl_mode = $(selector).data('rtl');

	$(selector).slick({
        centerMode: false,
        dots: false,
        rtl: rtl_mode,
        prevArrow: $('#carusel2-prev-'+id),
        nextArrow: $('#carusel2-next-'+id),
        centerPadding: '60px',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        infinite: true,
        adaptiveHeight: true,
	});

	$(selector).off('afterChange');
	$(selector).on('afterChange', function(event, slick, currentSlide, direction){
        var currSlide = $(slick.$slides[currentSlide]);
        var url = currSlide.data('url');

        $('#carusel-'+id+'pagInfo').html( (slick.currentSlide + 1 ) + '/' + slick.slideCount );	
        $('#carusel-'+id+'view-collection').attr("href", url);
	});
}

function fabio_carousel_simple_slider(selector){
	
    var id       = $(selector).data('uid');
    var rtl_mode = $(selector).data('rtl');

	$(selector).slick({
		centerMode: false,
		dots: false,
		centerPadding: '60px',
		slidesToShow: 5,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		arrows: false,
        rtl: rtl_mode,
		
	});	

	$(selector).off('afterChange');
	$(selector).on('afterChange', function(event, slick, direction){
		$('#carusel-'+id+'pagInfo').html( (slick.currentSlide +1)+'/'+slick.slideCount);
	});
}

$(document).ready(function ($) {

    $( '.hide_cart_widget_if_empty' ).closest( '.widget_shopping_cart' ).show();

    /* WC Header cart trigger  */
    if( $('#st-trigger-effects').length > 0 ) {
        $(document.body).on('click', '#st-trigger-effects, #close-modal-window', function(event) {
            event.preventDefault(); 
            $('#st-container').toggleClass('st-menu-open');
            event.stopPropagation(); 
        });

        //Hide when clicked outside
        $(document).mouseup(function (e){
            var container = $(".st-menu");
            if (!container.is(e.target) && container.has(e.target).length === 0){
                $('#st-container').removeClass('st-menu-open');
            }
        });
    }

    /* Footer trigger */
    if ( $('body').hasClass('footer-is-available') ) {
        $( ".footer-trigger" ).click(function(e) {
            e.preventDefault();
            $( ".footer-widgets-wrapper" ).slideToggle(200);
            $(this).parent().toggleClass("off");
        });

        if ( $('body').hasClass('footer-is-closed') ) {
            $( ".footer-trigger" ).trigger( "click" );
        }
    }

    /* Shop style 2 packery */
    if ($('body').hasClass('gg-shop-style_2')) {
        var $grid = $('.fabio-new-items-wrapper.products').imagesLoaded( function() {
            // init Packery after all images have loaded
            $grid.packery({
                itemSelector: '.packery-grid-item',
            });
        });
    }

    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
        event.preventDefault(); 
        event.stopPropagation(); 
        $(this).parent().siblings().removeClass('open');
        $(this).parent().toggleClass('open');
    });


    /* Mailchimp modal*/
    if( $('#fabio-modal-mailchimp').length > 0 && !$.cookie("newspopup-closed") ) { 
    	var delay = 0;
    	$('#fabio-modal-mailchimp .close').on('click',function(e){
        	$('#fabio-modal-mailchimp').fadeOut();
            $.cookie("newspopup-closed", 1, { expires : 30 });
    	})
    	if( parseInt($('#fabio-modal-mailchimp').data('delay')) > 0 )  {
            delay = parseInt($('#fabio-modal-mailchimp').data('delay')); 
    	}
    	
    	$('#fabio-modal-mailchimp').delay(delay).fadeIn();
    }


	/* FAQ */
	if($('.fabio-faq').length > 0){
		$('.fabio-faq .faq-title a').on('click touchend', function(e){
			var url = $(this).data('target');
			if($(this).data('expanded') == 'true'){
				$(this).data('expanded','false')
				$(url).slideUp();
				var span = $(this).find('span.fa');
					span.addClass('fa-minus')
					span.removeClass('fa-plus')
			} else {
				$(url).slideDown();
				$(this).data('expanded','true');
				var span = $(this).find('span.fa');
				span.removeClass('fa-minus')
				span.addClass('fa-plus')
			}
			e.preventDefault();
			return false;
		});
	} 
	
    //Nicescroll
	$(".whiteModal").getNiceScroll().hide();

	//carousel
	if($('.carousel-slick').length > 0) {
		$('.carousel-slick').each(function(){
			fabio_update_slick_slider(this);
		});
	}		
	/* Collection carousel */
	if($('.collection-carousel-slick').length > 0) {
		$('.collection-carousel-slick').each(function(){
			fabio_carousel_slick_collection(this);
		});
	}	
	if($('.fabio-simple-carousel').length > 0) {
		$('.fabio-simple-carousel').each(function(){
			fabio_carousel_simple_slider(this);
		});
	}		
	gg_slickcarousel_init();
    gg_magnific_init();
    gg_isotope_init();

    /* Countify */
	if($('.fabio_countify').length > 0){
		$('.fabio_countify').each(function(){
			$(this).Countify({});
		})
	}

/**increase the social share count number */
if($('.post-social .social-share-buttons li a' ).length > 0){
	$('.post-social .social-share-buttons li a' ).on('click tuchend', function(e){
		var pid = parseInt($(this).data('post-id'));
		jQuery.ajax({
			type: 'POST',
			url:ajax_object.ajax_url,
			data: { 
				action: "fabio_update_social_share_count", 
				post_id: pid
			},
			success: function(newCount){
			   $('#social-count-'+pid).html(newCount)
			}
		});				
		
	});
}	

// galleries popup 
if(	$( '.gallery-image a').length >0 ){
	$( '.gallery-image a').magnificPopup({type:'image'});	
}

    /* woocommerce results/page */
    if($('#fabio_woo_result_count').length  > 0){
    	$('#fabio_woo_result_count').on('change', function(){
    		window.location.href = fabio_update_parameter(window.location.href,'woo_per_page',$(this).val());
    	})
    }
	
    //WPML menu
    if ($('.sub-menu.submenu-languages').length > 0)  {
    	var submenu = $('.sub-menu.submenu-languages');
    	var menu = submenu.parent();

    	menu.addClass('gg-wpml-menu');
    	menu.find('a').addClass('dropdown-toggle').attr('data-toggle', 'dropdown');
    	submenu.addClass('dropdown-menu noclose');
    }

    //Hover navbar
	function hoverNavbar() {
		if ( $(window).width() > 992 && !$('body').hasClass('gg-theme-is-mobile') ) {
			
			$('.navbar-default .dropdown, .navbar-default .dropdown-submenu').on('mouseover', function(){
				$(this).addClass('open');

			}).on('mouseout', function(){
				$(this).removeClass('open');
			});
			
			$('.dropdown-toggle').on('click', function() {
				if ($(this).next('.dropdown-menu').is(':visible')) {
					window.location = $(this).attr('href');
				}
			});
		}
		else {
			$('.navbar-default .dropdown').off('mouseover').off('mouseout');
		}
	}
	//Load, resize, added to cart
	$(window).bind("load resize added_to_cart",function(e){
	 	hoverNavbar();
	});

    //Sticky menu
    function gg_sticky_menu() {
		if($('body.gg-has-stiky-menu').length > 0) {
			var main_menu = $('header.site-header .navbar');
			var main_menu_height = main_menu.outerHeight();
			var admin_bar = 0;

			if($('body.admin-bar').length > 0) {
				admin_bar = 32;
			}

       		$(window).on('scroll', function () {

       			if ($(this).scrollTop() > main_menu_height) {

       				main_menu.addClass('navbar-fixed-top');
       				$('body.gg-has-stiky-menu').css('padding-top', main_menu_height +'px');

       				setTimeout(function() {
					    $('header.site-header .navbar.navbar-fixed-top').css('top', admin_bar +'px');
					}, 500);

       			} else {

       				main_menu.removeClass('navbar-fixed-top');
       				$('body.gg-has-stiky-menu').css('padding-top', '0');
       				main_menu.css('top', '');

       			}

       		});
		}
	}

    //Load, resize, added to cart
	$(window).bind("load resize",function(e){
	 	gg_sticky_menu();
	});

        
	// here for the submit button of the comment reply form
	$( '#submit, input[type="button"], input[type="reset"], input[type="submit"], a.checkout-button' ).addClass( 'btn btn-primary' );	
	
	$( 'table' ).not('.variations, .cart').addClass( 'table');

	$( 'form' ).not('.header-search form, .variations_form').addClass( 'table');

	$( 'form' ).attr('role', 'form');

	var inputs = $('input, textarea')
            .not(':input[type=button], :input[type=submit], :input[type=reset]');

	$(inputs).each(function() {
	    $(this).addClass('form-control');
	});

	if($('body.gg-theme-is-mobile').length > 0) {
		$("a.product-image-overlay").on('click', function(event) {
		    event.preventDefault();
		});
	}

    //Currency switcher
	if($('.site-header .gg-currency-switcher').length > 0) {
	    $('.gg-currency-switcher ul.wcml_currency_switcher').addClass('dropdown-menu noclose');
    }

    //Initialize tooltips on page
    $(function () {
        $('[data-toggle="tooltip"]').tooltip(/*{delay: {show: 500, hide: 10000}}*/)
    })

    /* Search form trigger */
    $("a.top-search-trigger, a.overlay-close").click(function(e){
        e.preventDefault();
        $('.search-form-overlay').toggleClass("open");
    });

    /* Post 2 shortcode */
  	$(document).on('touchend click','.posts2 .ajaxLoadMore', function(e){
		var id = $(this).data('uid');
		$('#posts2_page'+id).val( parseInt($('#posts2_page'+id).val()) + 1);
		fabio_loadMorePost2(id,false,false,false);
		e.preventDefault();
	}) ;
	$(document).on('touchend click','.posts2 .posts_simple_pagination a.enabled', function(e){
		var id = $(this).data('uid');
		$('#posts2_page'+id).val($(this).data('page'));
		fabio_loadMorePost2(id,true,true,false);
		e.preventDefault();
	});
	$(document).on('touchend click','.posts2 .fabio-post1masonry-categories a', function(e){
		$('.posts2 .fabio-post1masonry-categories a').removeClass('active');
		$(this).addClass('active');
		var id = $(this).data('uid');
		$('#posts2_page'+id).val(1);
		$('#posts2_default_category'+id).val( $(this).data('catid') );
		fabio_loadMorePost2(id,true,false,true);
		e.preventDefault();
	});
	$(document).on('touchend click','.posts2 .fabio-post1SimplePagination-categories a', function(e){
		var id = $(this).data('uid');
		$('#posts2_page'+id).val(1);
		$('#posts2_default_category'+id).val( $(this).data('catid') );
		fabio_loadMorePost2(id,true,true,true);
		e.preventDefault();
	});	
    
    /* gallery shortcode */
	$(document).on('touchend click','.posts1 .ajaxLoadMore', function(e){
		var id = $(this).data('uid');
		$('#posts1_page'+id).val( parseInt($('#posts1_page'+id).val()) + 1);
		fabio_loadMorePost1(id,false,false,false);

		//Loading animation
		$(this).addClass('loading');
		e.preventDefault();
	});	
	$(document).on('touchend click','.posts1 .posts_simple_pagination a.enabled', function(e){
		var id = $(this).data('uid');
		$('#posts1_page'+id).val($(this).data('page'));
		fabio_loadMorePost1(id,true,true,false);
		e.preventDefault();
	});	
	$(document).on('touchend click','.posts1 .fabio-post1masonry-categories a', function(e){
		var id = $(this).data('uid');
		$('#posts1_page'+id).val(1);
		$('.posts1 .fabio-post1masonry-categories a').removeClass('active');
		$(this).addClass('active');
		$('#posts1_default_category'+id).val( $(this).data('catid') );
		fabio_loadMorePost1(id,true,false,true);
		e.preventDefault();
	});
	$(document).on('touchend click','.posts1 .fabio-post1SimplePagination-categories a', function(e){
		var id = $(this).data('uid');
		$('#posts1_page'+id).val(1);
		$('.posts1 .fabio-post1SimplePagination-categories a').removeClass('active');
		$(this).addClass('active');
		$('#posts1_default_category'+id).val( $(this).data('catid') );
		fabio_loadMorePost1(id,true,true);
		e.preventDefault();
	});	

    /*Gallery slick slider */
	
	if($('.gallery-slick-slider .gallery-image').length > 0){
		var col = parseInt($('.gallery-slick-slider').data('cols'));
		if(isNaN(col)) col = 1; 
		console.log(col);
		$('.gallery-slick-slider').slick({
            infinite: true,
            slidesToShow: col,
            slidesToScroll: col
		});
	}


    $(document).on('click touchend','.whiteModal .white-modal-item .close a ', function(e){
    		$(this).parent().parent().parent().removeClass('md-show');
    		$(".whiteModal").getNiceScroll().remove();
    		e.preventDefault();
    	});

		$('.upgfw-lightbox, .upgfw-wrapper .upgfw-image-gallery li:eq(0)').remove();
		$('.upgfw-wrapper .upgfw-image-gallery .upgfw-image-thumbnail img').click(function () {
			let currentImage = $(this).attr('src');
			let mainImage = $('.upgfw-product-image', $(this).parents('.upgfw-wrapper'));
			$('a img', mainImage).attr('src', currentImage);
		});
    });

function fabio_loadMorePost2(id,overwrite,isSimplePagination,isCategory){
	var $ = jQuery

	$('#posts_'+id).addClass('loading');
	if($('#posts2_show_categories'+id).val() == 'enable') {
		$('#ajax_'+id+'_loader').addClass('loading');
	}

	var data = {
		action: 'fabio_unyson_post',
		uid: id,
		page: parseInt($('#posts2_page'+id).val()),
		css_class: $('#posts2_css_class'+id).val(),
		current_post_id: $('#posts2_current_post_id'+id).val(),
		title: $('#posts2_title'+id).val(),
		subtitle: $('#posts2_subtitle'+id).val(),
		show_categories: $('#posts2_show_categories'+id).val(),
		pagination_type: $('#posts2_pagination_type'+id).val(),
		pagination_layout: $('#posts2_pagination_layout'+id).val(),
		per_page: ($('#posts2_per_page'+id).val()),
		columns: $('#posts2_columns'+id).val(),
		orderby:  $('#posts2_orderby'+id).val(),
		order:  $('#posts2_order'+id).val(),
		limit_category:  $('#posts2_limit_category'+id).val(),
		default_category:  $('#posts2_default_category'+id).val(),
		label_button:  $('#posts2_label_button'+id).val(),
		exclude_post:  $('#posts2_exclude_post'+id).val(),
		only_post:  $('#posts2_only_post'+id).val()
	};

	// We can also pass the url value separately from ajaxurl for front end AJAX implementations
	jQuery.post(ajax_object.ajax_url, data, function(response) {
		var posts_count_1 = data.page * data.per_page;

		$('#posts_'+id).removeClass('loading');
		
		if($('#posts2_show_categories'+id).val() == 'enable') {
			$('#ajax_'+id+'_loader').removeClass('loading');
		}

		$('#posts_'+id).find('.ajaxLoadMore').removeClass('loading');

		if( response.html ) {
			if( data.page < response.post_count )	{
				
                if($('#posts_'+id+' .ajaxLoadMorePosts').length > 0){
					$('#posts_'+id+' .ajaxLoadMorePosts').slideDown();
					$('#posts_'+id+' .ajaxLoadMorePosts').removeClass('hidden');
				}

				if($('#posts_'+id+' .ajaxLoadMore').length > 0 ){
					$('#posts_'+id+' .ajaxLoadMore').slideDown();
					$('#posts_'+id+' .ajaxLoadMore').removeClass('hidden');
				}

			} else {
                $('#posts_'+id+' .ajaxLoadMorePosts').addClass('hidden');		
			}
			// it is a category
			if(overwrite) {
		
				// update the button and slide up the button, just in case is hidden
				if(data.pagination_layout == 'carousel'){
					$($('#carusel-'+id)).slick("unslick");
					$('#carusel-'+id).html(response.html);
					fabio_update_slick_slider($('#carusel-'+id));
				} else {
					$('#posts_'+id+' .posts1_cnt').html(response.html);
				} 

			} else { 
		
				$('#posts_'+id+' .posts1_cnt').append(response.html);
			}

			if(!data.pagination_layout != 'carousel_pagination'){
				jQuery('#posts_'+id+' .masonry_post').imagesLoaded( function() {
					updateMasonry(jQuery('#posts_'+id+' .masonry_post'),'.masonry_post_item');
					if($('#posts_'+id+' .'+response.first).length > 0 && !isCategory){
						fabio_scroll2('#posts_'+id+' .'+response.first);
					}
				}) ;
			}						
		}


	},'json');
}

/*pagination for load more style **/
function fabio_loadMorePost1(id,overwrite,isSimplePagination,isCategory){
	var $ = jQuery;

	$('#posts_'+id).addClass('loading');
	if($('#posts1_show_categories'+id).val() == 'enable') {
		$('#ajax_'+id+'_loader').addClass('loading');
	}

	var data = {
				action: 'fabio_unyson_gallery',
				uid: id,
				page: $('#posts1_page'+id).val(),
				css_class: $('#posts1_css_class'+id).val(),
				current_post_id: $('#posts1_current_post_id'+id).val(),
				title: $('#posts1_title'+id).val(),
				subtitle: $('#posts1_subtitle'+id).val(),
				show_categories: $('#posts1_show_categories'+id).val(),
				pagination_type: $('#posts1_pagination_type'+id).val(),
				pagination_layout: $('#posts1_pagination_layout'+id).val(),
				per_page: $('#posts1_per_page'+id).val(),
				columns: $('#posts1_columns'+id).val(),
				orderby:  $('#posts1_orderby'+id).val(),
				order:  $('#posts1_order'+id).val(),
				limit_category:  $('#posts1_limit_category'+id).val(),
				default_category:  $('#posts1_default_category'+id).val(),
				label_button:  $('#posts1_label_button'+id).val(),
				exclude_post:  $('#posts1_exclude_post'+id).val(),	
				only_post:  $('#posts1_only_post'+id).val()
			};

			// We can also pass the url value separately from ajaxurl for front end AJAX implementations
			jQuery.post(ajax_object.ajax_url, data, function(response) {

				$('#posts_'+id).removeClass('loading');
				
				if($('#posts1_show_categories'+id).val() == 'enable') {
					$('#ajax_'+id+'_loader').removeClass('loading');
				}

				$('#posts_'+id).find('.ajaxLoadMore').removeClass('loading');
				
				if(response.html){// it is a category
				
					if(data.page < response.post_count )	{
						
						if($('#posts_'+id+' .ajaxLoadMorePosts').length > 0){
							$('#posts_'+id+' .ajaxLoadMorePosts').slideDown();
							$('#posts_'+id+' .ajaxLoadMorePosts').removeClass('hidden');		
						}
						

					}else{
				
							$('#posts_'+id+' .ajaxLoadMorePosts').addClass('hidden');		
						}
					if(overwrite) {
						
						// update the button and slide up the button, just in case is hidden
						$('#posts_'+id+' .posts1_cnt').html(response.html);
						$('#posts_'+id+' .ajaxLoadMorePosts').slideDown();	
					
						if(data.pagination_layout == 'carousel'){
							$($('#carusel-'+id)).slick("unslick");
							fabio_update_slick_slider($('#carusel-'+id));
						} 
						
					} else { 
					// just a simple ajax
						$('#posts_'+id+' .posts1_cnt').append(response.html);
					}
				}
				// if is masonry layout then update it else ignore it
					if(!isSimplePagination){
						jQuery('#posts_'+id+' .masonry_post').imagesLoaded( function() {
							updateMasonry(jQuery('#posts_'+id+' .masonry_post'),'.masonry_post_item');
							if(response.first != '' && !isCategory )  fabio_scroll2('#posts_'+id+' .'+response.first);
						}) ;
					}
			},'json');
}

/* get content for a category **/
if($('.fabio-categories a').length > 0){
	
	$('.fabio-categories a').on('click touchend', function(e){
		
		$('#'+$(this).parent().attr('id')+' a').removeClass('current');
		
		$(this).addClass('current');
		var ajax_id = $(this).attr('href').split('#ajax_');
		
		$('#ajax_page'+ajax_id[1]).val(0);
		$('#ajax_default_category'+ajax_id[1]).val($(this).data('id') );
		
	    fabio_unyson_getProducts(ajax_id[1],true,true,true )
	
		e.preventDefault();
	});
}

//grid navigation 
if($('.product_grid a').length > 0){
	$(document).on('click touchend','.product_grid a', function(e){
		if($(this).hasClass('disabled')) return false;
		var uid = $(this).data('uid');
		$('#ajax_page'+uid).val($(this).data('page') );
		fabio_unyson_getProducts(uid ,true,false,false);
		e.preventDefault();
	});
}

	
/* get content for a collection category **/
if($('.fabio-collection-categories a').length > 0){
	
	$('.fabio-collection-categories a').on('click touchend', function(e){
		
		$('#'+$(this).parent().attr('id')+' a').removeClass('current');
		
		$(this).addClass('current');
		var ajax_id = $(this).attr('href').split('#ajax_');
		
		$('#ajax_page'+ajax_id[1]).val(0);
		$('#ajax_default_category'+ajax_id[1]).val($(this).data('id') );
		

	    fabio_unyson_getCollections(ajax_id[1],true,true,true )
	
		e.preventDefault();
	});
}

//grid navigation 
if($('.collection_grid a').length > 0){
	$(document).on('click touchend','.collection_grid a', function(e){
		if($(this).hasClass('disabled')) return false;
		var uid = $(this).data('uid');
		$('#ajax_page'+uid).val($(this).data('page') );
		fabio_unyson_getCollections(uid ,true,false,false);
		e.preventDefault();
	});
}

if($('.ajaxLoadMoreProducts a').length > 0){
	$(document).on('click touchend','.ajaxLoadMoreProducts a', function(e){
	
		var uid = $(this).data('uid');

		$('#ajax_page'+uid).val( parseInt($('#ajax_page'+uid).val())+1 );
		fabio_unyson_getProducts(uid,false,false,false );
		
		//Loading animation
		$('#ajax_'+uid).parent().removeClass('loading');
		$(this).addClass('loading');

		e.preventDefault();
	});
}

if($('.blog_grid a').length > 0){
	$(document).on('click touchend','.blog_grid a', function(e){
		if($(this).hasClass('disabled')) return false;
		var uid = $(this).data('uid');
		$('#ajax_page'+uid).val($(this).data('page') );
		fabio_unyson_getBlogs(uid );
		 e.preventDefault();
	});
}


/*ajax products*/
function fabio_unyson_getProducts(unique_id,overwrite,showAjaxButton, is_category){
	var $ = jQuery;

	$('#ajax_'+unique_id).parent().addClass('loading');
	
	if($('#ajax_show_filters'+unique_id).val() == 'enable') {
		$('#ajax_'+unique_id+'_loader').addClass('loading');
	}

	var data = {
				action: 'fabio_unyson_products',
				category: $('#ajax_default_category'+unique_id).val(),
				current_post_id: $('#ajax_current_post_id'+unique_id).val(),
				css_class: $('#ajax_css_class'+unique_id).val(),
				exclude_products: $('#ajax_exclude_products'+unique_id).val(),
				only_post: $('#ajax_only_post'+unique_id).val(),
				title: $('#ajax_title'+unique_id).val(),
				subtitle: $('#ajax_subtitle'+unique_id).val(),
				style: $('#ajax_style'+unique_id).val(),
				pagination_type: $('#ajax_pagination_type'+unique_id).val(),
				per_page: $('#ajax_per_page'+unique_id).val(),
				columns:  $('#ajax_columns'+unique_id).val(),
				orderby:  $('#ajax_orderby'+unique_id).val(),
				label_button:  $('#ajax_label_button'+unique_id).val(),
				order:  $('#ajax_order'+unique_id).val(),
				show_filters:  $('#ajax_show_filters'+unique_id).val(),
				show_on_sale:  $('#ajax_show_on_sale'+unique_id).val(),
				default_category:  $('#ajax_default_category'+unique_id).val(),
				show_most_popular:  $('#ajax_show_most_popular'+unique_id).val(),
				show_new_items:  $('#ajax_show_new_items'+unique_id).val(),
				show_categories:  $('#ajax_show_categories'+unique_id).val(),
				show_featured:  $('#ajax_show_featured'+unique_id).val(),
				limit_category:  $('#ajax_limit_category'+unique_id).val(),
				show_only_categories:  $('#ajax_show_only_categories'+unique_id).val(),
				enable_arrow_pagination:$('#ajax_enable_arrow_pagination'+unique_id).val(),
				enable_dots_pagination:$('#ajax_enable_dots_pagination'+unique_id).val(),
				page:  $('#ajax_page'+unique_id).val(),
				showAjaxButton:showAjaxButton,
				uid: unique_id
			};

			// We can also pass the url value separately from ajaxurl for front end AJAX implementations
			jQuery.post(ajax_object.ajax_url, data, function(response) {

				
				$('#ajax_'+unique_id).parent().removeClass('loading');

				if($('#ajax_show_filters'+unique_id).val() == 'enable') {
					$('#ajax_'+unique_id+'_loader').removeClass('loading');
				}

				if ( data.style == 'carousel' ) {
					$($('#carusel-'+unique_id)).slick("unslick");
				}

				if (overwrite) {
					if(data.pagination_type == 'simple_pagination'){
						$('#ajax_'+unique_id).html(response.html);
					}else{
						$('#ajax_'+unique_id+' .products-carousel').html(response.html);
					}
				
				} else {
	
					$('#ajax_'+unique_id+' .products-carousel').append(response.html);
					
				}
					
				if (data.style == 'carousel') {
					$('#ajax_'+unique_id+'').html(response.html);
					fabio_update_slick_slider($('#carusel-'+unique_id));
				} else {
					jQuery('#ajax_'+unique_id+' .masonry_post').imagesLoaded( function() {
						updateMasonry(jQuery('#ajax_'+unique_id+' .masonry_post'),'#ajax_'+unique_id+' .masonry_post_item');
						jQuery('#ajax_'+unique_id).find('.ajaxLoadMore').removeClass('loading');
						if(response.first != '' && !is_category)  fabio_scroll2('#ajax_'+unique_id+' .'+response.first);
					}) ;
				}

			},'json');
}


/*ajax collection*/
function fabio_unyson_getCollections(unique_id,overwrite,showAjaxButton, is_category){
	var $ = jQuery;

	$('#ajax_'+unique_id).parent().addClass('loading');
	
	if($('#ajax_show_filters'+unique_id).val() == 'enable') {
		$('#ajax_'+unique_id+'_loader').addClass('loading');
	}

	var data = {
				action: 'fabio_unyson_collections',
				category: $('#ajax_default_category'+unique_id).val(),
				current_post_id: $('#ajax_current_post_id'+unique_id).val(),
				css_class: $('#ajax_css_class'+unique_id).val(),
				exclude: $('#ajax_exclude'+unique_id).val(),
				only_post: $('#ajax_only_post'+unique_id).val(),
				title: $('#ajax_title'+unique_id).val(),
				subtitle: $('#ajax_subtitle'+unique_id).val(),
				style: $('#ajax_style'+unique_id).val(),
				pagination_type: $('#ajax_pagination_type'+unique_id).val(),
				per_page: $('#ajax_per_page'+unique_id).val(),
				columns:  $('#ajax_columns'+unique_id).val(),
				orderby:  $('#ajax_orderby'+unique_id).val(),
				label_button:  $('#ajax_label_button'+unique_id).val(),
				order:  $('#ajax_order'+unique_id).val(),
				show_filters:  $('#ajax_show_filters'+unique_id).val(),
				default_category:  $('#ajax_default_category'+unique_id).val(),
				show_most_popular:  $('#ajax_show_most_popular'+unique_id).val(),
				show_new_items:  $('#ajax_show_new_items'+unique_id).val(),
				show_categories:  $('#ajax_show_categories'+unique_id).val(),
				show_featured:  $('#ajax_show_featured'+unique_id).val(),
				limit_category:  $('#ajax_limit_category'+unique_id).val(),
				show_only_categories:  $('#ajax_show_only_categories'+unique_id).val(),
				enable_arrow_pagination:$('#ajax_enable_arrow_pagination'+unique_id).val(),
				enable_dots_pagination:$('#ajax_enable_dots_pagination'+unique_id).val(),
				page:  $('#ajax_page'+unique_id).val(),
				showAjaxButton:showAjaxButton,
				uid: unique_id
			};

			// We can also pass the url value separately from ajaxurl for front end AJAX implementations
			jQuery.post(ajax_object.ajax_url, data, function(response) {

				
				$('#ajax_'+unique_id).parent().removeClass('loading');

				if($('#ajax_show_filters'+unique_id).val() == 'enable') {
					$('#ajax_'+unique_id+'_loader').removeClass('loading');
				}

				if ( data.style == 'carousel' ) {
					$($('#carusel-'+unique_id)).slick("unslick");
				}

				if (overwrite) {
					if(data.pagination_type == 'simple_pagination'){
						$('#ajax_'+unique_id).html(response.html);
					}else{
						$('#ajax_'+unique_id+' .products-carousel').html(response.html);
					}
				
				} else {
	
					$('#ajax_'+unique_id+' .products-carousel').append(response.html);
					
				}
					
				if (data.style == 'carousel') {
					$('#ajax_'+unique_id+'').html(response.html);
					fabio_update_slick_slider($('#carusel-'+unique_id));
				} else {
					jQuery('#ajax_'+unique_id+' .masonry_post').imagesLoaded( function() {
						updateMasonry(jQuery('#ajax_'+unique_id+' .masonry_post'),'#ajax_'+unique_id+' .masonry_post_item');
						jQuery('#ajax_'+unique_id).find('.ajaxLoadMore').removeClass('loading');
						if(response.first != '' && !is_category)  fabio_scroll2('#ajax_'+unique_id+' .'+response.first);
					}) ;
				}

			},'json');
}

/*New loader*/
$(document).ready(function ($) {

var $content = $('#fabio_post_items');
var $loader  = $('#fabio-archive-load-more');
var cat      = $loader.data('category');
var ppp      = parseInt($loader.data('pagecount'));
var pageid   = parseInt($loader.data('pageid'));
var offset   = $content.find('.post-item').length;
 
$loader.on( 'click', load_ajax_posts );
 
function load_ajax_posts() {
    if (!($loader.hasClass('post_loading_loader') || $loader.hasClass('post_no_more_posts'))) {
       
        $.ajax({
            type: 'POST',
            dataType: 'html',
            url: __fabio__.ajax_url,
            data: {
                'cat': cat,
                'ppp': ppp,
                'offset': offset,
                'pageid': pageid,
                'action': 'fabio_more_post_ajax'
            },
            beforeSend : function () {
                $loader.addClass('post_loading_loader').html(__fabio__.loading);
            },
            success: function (data) {
                var $data = $(data);
                if ($data.length) {
                    var $newElements = $data.css({ opacity: 0 });
                    $content.append( $newElements ).isotope( 'appended', $newElements );
                    // trigger the isotope layout after all images are loaded
                    // http://imagesloaded.desandro.com/#jquery
                    $content.imagesLoaded().always(function() {
                        $content.isotope('layout');
                    });
                    $newElements.animate({ opacity: 1 });
                    
                    $loader.removeClass('post_loading_loader').html(__fabio__.loadmore);
                } else {
                    $loader.removeClass('post_loading_loader').addClass('post_no_more_posts').html(__fabio__.noposts);
                }
            },
            error : function (jqXHR, textStatus, errorThrown) {
                $loader.html($.parseJSON(jqXHR.responseText) + ' :: ' + textStatus + ' :: ' + errorThrown);
                console.log(jqXHR);
            },
        });
    }
    offset += ppp;
    return false;
}

});

})(jQuery);

function fabio_unyson_getBlogs(unique_id){
	var $ = jQuery;
	$('#ajax_'+unique_id+'_loader').fadeIn();
	var data = {
				action: 'fabio_unyson_posts',
				unique_id: unique_id,
				category: $('#ajax_default_category'+unique_id).val(),
				css_class: $('#ajax_css_class'+unique_id).val(),
				exclude_products: $('#ajax_exclude_products'+unique_id).val(),
				title: $('#ajax_title'+unique_id).val(),
				enable_thumbs: $('#ajax_enable_thumbs'+unique_id).val(),
				subtitle: $('#ajax_subtitle'+unique_id).val(),
				style: $('#ajax_style'+unique_id).val(),
				per_page: $('#ajax_per_page'+unique_id).val(),
				columns:  $('#ajax_columns'+unique_id).val(),
				orderby:  $('#ajax_orderby'+unique_id).val(),
				order:  $('#ajax_order'+unique_id).val(),
				default_category:  $('#ajax_default_category'+unique_id).val(),
				limit_category:  $('#ajax_limit_category'+unique_id).val(),
				page:  $('#ajax_page'+unique_id).val()
			};
		
			// We can also pass the url value separately from ajaxurl for front end AJAX implementations
			jQuery.post(ajax_object.ajax_url, data, function(response) {
				$('#ajax_page'+unique_id).val(1 );
				$('#ajax_'+unique_id+'_loader').hide();
				$('#ajax_'+unique_id).html(response);
			});
}

/** convert svg img to svg */

jQuery('img.svg').each(function(){
	var $img = jQuery(this);
	var imgID = $img.attr('id');
	var imgClass = $img.attr('class');
	var imgURL = $img.attr('src');

	jQuery.get(imgURL, function(data) {
		// Get the SVG tag, ignore the rest
		var $svg = jQuery(data).find('svg');

		// Add replaced image's ID to the new SVG
		if(typeof imgID !== 'undefined') {
			$svg = $svg.attr('id', imgID);
		}
		// Add replaced image's classes to the new SVG
		if(typeof imgClass !== 'undefined') {
			$svg = $svg.attr('class', imgClass+' replaced-svg');
		}

		// Remove any invalid XML tags as per http://validator.w3.org
		$svg = $svg.removeAttr('xmlns:a');

		// Replace image with new SVG
		$img.replaceWith($svg);

	}, 'xml');

});


function updateMasonry(selector,itemSelector){

	if(selector.data('layout-mode')) {
		selector.isotope('destroy');
	}
	
	var layout =  selector.data('layout-mode'); 
	if(layout == undefined) {
		layout = 'fitRows';
	} 

	selector.isotope({
		// options
		itemSelector: itemSelector ,
		layoutMode: layout
	});


}

function fabio_update_parameter(url, param, value) {
    // Using a positive lookahead (?=\=) to find the
    // given parameter, preceded by a ? or &, and followed
    // by a = with a value after than (using a non-greedy selector)
    // and then followed by a & or the end of the string
    var val = new RegExp('(\\?|\\&)' + param + '=.*?(?=(&|$))'),
        parts = url.toString().split('#'),
        url = parts[0],
        hash = parts[1]
        qstring = /\?.+$/,
        newURL = url;

    // Check if the parameter exists
    if (val.test(url))
    {
        // if it does, replace it, using the captured group
        // to determine & or ? at the beginning
        newURL = url.replace(val, '$1' + param + '=' + value);
    }
    else if (qstring.test(url))
    {
        // otherwise, if there is a query string at all
        // add the param to the end of it
        newURL = url + '&' + param + '=' + value;
    }
    else
    {
        // if there's no query string, add one
        newURL = url + '?' + param + '=' + value;
    }

    if (hash)
    {
        newURL += '#' + hash;
    }

    return newURL;
}

function fabio_scroll2( selector){
	if(selector =='') return;
	jQuery('html,body').animate({
		scrollTop: jQuery(selector).offset().top -100 },
    'slow');	
}

function goto(href) {
	document.location.href = href;
}