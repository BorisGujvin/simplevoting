<?php
/**
 * Default Post Template
 *
 * @package WordPress
 * @subpackage fabio
 */
get_header(); ?>



<?php while (have_posts()) : the_post(); ?>

<section id="content">
    <div class="container">
        <div class="row">
            <div class="<?php fabio_page_container(); ?>">

                <?php get_template_part( 'parts/post-formats/part', get_post_format() ); ?>

                <?php endwhile; // end of the loop. ?>
                
                <?php comments_template( '', true ); ?>

            </div><!-- end page container -->
            <?php fabio_page_sidebar(); ?>

        </div><!-- /.row -->
    </div><!--/.container -->    
</section>

<?php get_footer(); ?>