<?php
/**
 * Default Gallery Post Template
 *
 * @package WordPress
 * @subpackage fabio
 */
get_header(); ?>

<section id="content">
    <div class="container">
        <div class="row">
            <div class="<?php fabio_page_container(); ?>">

                <?php while (have_posts()) : the_post(); ?>
                    
                <?php get_template_part( 'parts/seasons/part', 'collection' ); ?>

                <?php endwhile; // end of the loop. ?>
                          
            </div><!-- end page container -->
            <?php fabio_page_sidebar(); ?>

        </div>
    </div>    
</section>

<?php get_footer(); ?>