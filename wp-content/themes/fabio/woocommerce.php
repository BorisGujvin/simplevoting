<?php
/**
 * WooCommerce
 * Description: Page template for WooCommerce
 *
 * @package WordPress
 * @subpackage fabio
 */
get_header(); ?>

<?php

$shop_style = _get_field('gg_shop_style','option', 'style_1');
if ( isset( $_GET['shop_style'] ) ) {
    $shop_style = $_GET['shop_style'];
}
?>

<section id="content">

    <?php if ( is_product() ) : ?>

        <?php woocommerce_content(); ?>
        <?php fabio_page_sidebar('special_page'); ?>

    <?php else: ?>

        <div class="<?php echo ( $shop_style == 'style_2' ) ? 'container-fluid' : 'container'; ?>">
            <div class="row">
                <div class="<?php fabio_page_container('special_page'); ?>">
                    <?php woocommerce_content(); ?>
                </div>

                <?php fabio_page_sidebar('special_page'); ?>
            </div>
        </div>

    <?php endif; ?>


</section>

<?php get_footer(); ?>