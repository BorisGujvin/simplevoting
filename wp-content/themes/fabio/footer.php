<?php
/**
 * Footer
 *
 * @package WordPress
 * @subpackage fabio
 */
?>
  </div> <!-- end content wrapper -->  
    <footer class="site-footer">
        <?php $footercount = is_active_sidebar('sidebar-footer-first') + is_active_sidebar('sidebar-footer-second') + is_active_sidebar('sidebar-footer-third') + is_active_sidebar('sidebar-footer-fourth'); ?>
        
        <?php if ($footercount) : ?>        
        <div class="footer-trigger-wrapper">
            <a href="#" class="footer-trigger"></a>
        </div>
        
        <?php
            $global_footer = _get_field('gg_footer_widgets', 'option', true);
            $page_footer = _get_field('gg_page_footer', fabio_global_page_id(),true);
        ?>
        
        <?php if ( $global_footer && $page_footer ): ?>
        <div class="footer-widgets-wrapper">
            <div class="container">
                <div class="row">
                    <div class="footer-widgets col-md-12">
                        <?php get_sidebar("footer"); ?>
                    </div>
                </div><!-- .row -->
            </div><!-- /.container --> 
        </div><!-- /.footer-widgets -->
        <?php endif; ?>

        <?php endif; ?>
		
        <!-- logo footer and social icons -->
		<div class="footer-logo-sm">
			<div class="container">
				<div class="row">
					
          <span class="logo"><?php get_template_part( 'parts/part-header-logo');?></span>

          <ul class="footer-social-icons-widget">
            <li><a href="https://www.instagram.com/honaydaofficial/" target="_blank"><i class="fa fa-instagram"></i></a></li>
            <li><a href="http://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i></a></li>
          </ul>
					<?php if (_get_field('gg_social_icons','option')) : ?>
					
						<ul class="footer-social-icons-widget">
							<?php
								while (has_sub_field('gg_social_icons','option')) { //Loop through sidebar fields to generate custom sidebars

									$s_name = get_sub_field('gg_social_icon_name','option');
									$s_icon = get_sub_field('gg_select_social_icon','option');
									$s_link = get_sub_field('gg_social_icon_link','option');
									echo '<li><a href="'.esc_url($s_link).'" target="_blank"><i class="'.esc_attr($s_icon).'"></i></a></li>';
								}
							?>
						</ul>
	
					<?php endif; ?>
					
					<div class="footer-menu">
								<!-- Begin Main Navigation -->
						<?php
						wp_nav_menu(
							array(
								'theme_location'    => 'footer-menu',
								'container'         => '',
								'container_class'   => '',
								'menu_class'        => 'nav navbar-nav',
								'fallback_cb'       => 'fabio_navwalker::fallback',
								'menu_id'           => 'footer-menu',
								'walker'            => new fabio_navwalker()
							)
						); ?>
						<!-- End Main Navigation -->
						</div>
				</div>
			</div>
			
		</div>	
		<!-- end logo footer and social icons -->
        
        <?php fabio_footer_extras(); ?>

    </footer>
				<!-- the content -->
			</div><!-- /st-content-inner -->
		</div><!-- /st-content -->

	</div><!-- /st-pusher -->
</div>

    <?php if( _get_field('gg_header_search', 'option', true) ) : ?>
        <div class="search-form-overlay">
                <div class="col-md-6 col-md-offset-3">
                    <a href="#" class="overlay-close"><?php esc_html_e('Close','fabio'); ?></a>
                    <form method="get" id="searchformoverlay" class="" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <input class="form-control" type="text" value="<?php the_search_query(); ?>" placeholder="<?php esc_attr_e( 'Search', 'fabio' ); ?>" name="s" id="s" />
                        <input class="btn btn-primary" type="submit" id="searchsubmit" value="<?php esc_attr_e( 'Go', 'fabio' ); ?>" />
                    </form>
                </div>
        </div>
    <?php endif; ?>

    <?php wp_footer(); ?>
    </body>
</html>