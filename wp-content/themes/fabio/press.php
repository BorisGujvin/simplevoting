<?php
/**
  Template Name: Press Page
 */

get_header(); ?>


<?php
//Get the ID of the page used for Blog posts
$page_id = ( 'page' == get_option( 'show_on_front' ) ? get_option( 'page_for_posts' ) : get_the_ID() );

$css_class         = ( 'page' == get_option( 'show_on_front' ) ?'fabio_blog' : 'fabio_page' );
$blog_layout       = _get_field('gg_blog_layout',$page_id,'masonry');
$blog_layout_style = _get_field('gg_blog_layout_style',$page_id,'gap');
$blog_columns      = _get_field('gg_blog_columns',$page_id,3);
$blog_no_posts     = _get_field('gg_blog_no_of_posts_to_show',$page_id,5);
$blog_pagination   = _get_field('gg_blog_pagination',$page_id,'numbered');

global $post;
$post_slug = $post->post_name;
?>

<section id="content" class="<?php echo esc_attr($css_class); ?>">
    <div class="container">
        <div class="row">
          <div class="<?php fabio_page_container($page_id); ?>">

            <div class="gg_posts_grid ">
                <?php if ( have_posts() ) : ?>

                    <?php
                    $args = array( 'post_type' => 'post', 'posts_per_page' => -1, 'cat' => '63', 'orderby' => 'date', 'order' => 'DESC' );
                    $loop = new WP_Query( $args );?>
                    <ul id="fabio_post_items" class="masonry_post el-grid post-arhive <?php if($blog_layout_style == 'nogap') echo 'nogap-cols'; ?>" data-layout-mode="<?php echo esc_attr($blog_layout); ?>" data-gap="<?php echo esc_attr($blog_layout_style); ?>" data-pagination="<?php echo esc_attr($blog_pagination); ?>" data-columns="<?php echo esc_attr($blog_columns); ?>">
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <li class="isotop-grid-item isotope-item post-item col-xs-12 col-md-<?php echo esc_attr(floor( 12 / $blog_columns )); ?>"><?php get_template_part( 'parts/post-formats/part', get_post_format() ); ?></li>
                    <?php endwhile; ?>
                    </ul>

                    <?php if ($blog_pagination == 'ajax_load') : ?>
                    <div class="pagination-load-more">
                            <span class="pagination-span">
                                <?php echo  fabio_ajax_posts_link( esc_html__('Load more posts','fabio'), $wp_query->max_num_pages, $page_id) ?>
                            </span>
                    </div>
                    <?php else : ?>
                        <?php if (function_exists("fabio_pagination")) fabio_pagination(); ?>
                    <?php endif; ?>

                <?php else : ?>

                    <?php get_template_part( 'parts/post-formats/part', 'none' ); ?>

                <?php endif; // end have_posts() check ?>

            <?php fabio_page_sidebar($page_id); ?>

            </div>
          </div>
        </div><!-- .row -->
    </div><!-- .container -->
</section>
<?php get_footer(); ?>