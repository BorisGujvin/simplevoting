<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			$args = array( 'post_type' => 'post', 'posts_per_page' => -1, 'cat' => '15', 'orderby' => 'date', 'order' => 'ASC' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="Bloc-580 <?php echo "post-".get_the_ID(); ?>">
					<?php twentysixteen_post_thumbnail(); ?>
					<?php echo apply_filters( 'the_content',get_the_content()); ?>
				</div>
		<?php endwhile; ?>
		<?php wp_reset_query(); ?>
	</div><!-- .entry-content -->

	<div class="footer">
	
	</div>

</article><!-- #post-## -->