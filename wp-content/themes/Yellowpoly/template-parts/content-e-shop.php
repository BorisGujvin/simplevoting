<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->


	<div class="entry-content">
		<?php
		
/* 		$args = array(
			'post_type' => 'attachment',
			'post_status' => 'any',
			'tax_query' => array(
				array(
					'taxonomy' => 'media_category', // your taxonomy
					'field' => 'id',
					'terms' => 23 // term id (id of the media category)
				)
			)
		);
		$the_query = new WP_Query( $args );
		 
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				//echo wp_get_attachment_image( get_the_ID(), 'full' );
				echo get_the_post_thumbnail( get_the_ID(), 'full' );
				var_dump(the_post());
			}
		}
		else {
			// no attachments found
		}
		 
		wp_reset_postdata(); */
		
 		$args = array( 'post_type' => 'post', 'posts_per_page' => -1, 'cat' => '19', 'orderby' => 'date', 'order' => 'ASC' );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="Bloc-Tiers">
				<h3><?php the_title(); ?></h3>
				<?php twentysixteen_post_thumbnail(); ?>
			</div>
	<?php endwhile; ?>
		<?php wp_reset_query(); ?>
		
	</div><!-- .entry-content -->

	<div class="footer">
	
	</div>

</article><!-- #post-## -->