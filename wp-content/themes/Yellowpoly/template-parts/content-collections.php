<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php //twentysixteen_post_thumbnail(); ?>
	<div class="entry-content">
		<div class="top-collections"><?php the_content(); ?></div>
	<?php	/* 
			$args = array( 'post_type' => 'post', 'posts_per_page' => -1, 'cat' => '17', 'orderby' => 'date', 'order' => 'ASC' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="Bloc-580">
					<p><?php
					if( function_exists( 'easy_image_gallery' ) ) {
						echo easy_image_gallery();
					};
					?></p>
				</div>
		<?php endwhile; ?>
		<?php wp_reset_query(); */ ?>
	</div><!-- .entry-content -->

	<div class="footer">
	
	</div>

</article><!-- #post-## -->