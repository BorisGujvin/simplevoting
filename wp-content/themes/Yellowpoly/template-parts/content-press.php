<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
	<ul class="Bloc-Press">
	<?php
	$args = array( 'post_type' => 'post', 'posts_per_page' => -1, 'cat' => '63', 'orderby' => 'date', 'order' => 'DESC' );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<?php
				$small = get_the_post_thumbnail_url($post->ID,'medium');
				$url = get_the_post_thumbnail_url($post->ID,'full');
				echo "<li class='pagepress'><a href='".$url."' target='_blank'><img src='".$small."'><div class='infos'><h2>".$post->post_title."</h2><p>".$post->post_content."</p></div></a></li>";
			?>	
	<?php endwhile; ?>
	</ul>
		<?php wp_reset_query(); ?>
	</div><!-- .entry-content -->

	<div class="footer">
	
	</div>

</article><!-- #post-## -->
<script>
$(document).ready(function() {
	
	var $grid = $('.Bloc-Press').isotope({
	  // options
	  itemSelector: '.pagepress',
	  layoutMode: 'masonry'
	});
	
	$grid.imagesLoaded().progress( function() {
	  $grid.isotope('layout');
	});
	
});
</script>