</div><!-- .site-content -->

<footer id="colophon" class="site-footer" role="contentinfo">

  <div class="site-info">
    <div class="foot-div menu">
    <ul>
      <li>
        <a href="http://www.honayda.com/honayda-designer/" rel="honayda-designer">
          <?php bloginfo( 'name' ); ?>
         | DESIGNER </a></li>
        <li>
          <a href="http://www.honayda.com/policies/" rel="policies">POLICIES</a>
        </li>
        <li>
          <a href="http://www.honayda.com/contact/" rel="contact">CONTACT</a>
        </li>
    </ul>
    </div>

  </div><!-- .site-info -->
</footer><!-- .site-footer -->
</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>