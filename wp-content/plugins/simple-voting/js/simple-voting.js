jQuery(document).ready(function(){
    jQuery(".simple-voting-vote-button, .simple-voting-unvote-button").click(function(e){
        var vb=jQuery(this);
        var votefor=vb.attr('votedata');
        var data1 = {
            action: 'simple_vote',
            votefor: votefor
        };
        vb.unbind('hover');
        vb.html('voting...');
        jQuery.ajax({
            url:ajaxurl,
            type:'POST',
            data:data1,
            success: function (data){
                var i =  vb.attr('class');
                if(i=="simple-voting-vote-button") {
                    vb.removeClass('simple-voting-vote-button');
                    vb.addClass('simple-voting-unvote-button');
                    var t =vb.attr('votedata');
                    vb.attr('votedata' , (-1)*t);
                    vb.html('Liked');
                } else{
                    vb.removeClass('simple-voting-unvote-button');
                    vb.addClass('simple-voting-vote-button');
                    vb.html('Like');
                    vb.attr('votedata',(-1)*vb.attr('votedata'));
                }
            }
        });

        vb.blur();
        e.preventDefault();
    });
});
