<?php
/*
Plugin Name: Голосовалка
Plugin URI: http://страница_с_описанием_плагина_и_его_обновлений
Description: Краткое описание плагина.
Version: Номер версии плагина, например: 1.0
Author: Имя автора плагина
Author URI: http://страница_автора_плагина
*/

class Simple_voting{

    public static $instance;

    public function createTable(){
        global $wpdb;
        $prefix = $wpdb->get_blog_prefix();
        $creation_query =
            'CREATE TABLE IF NOT EXISTS ' . $prefix . 'voting (`id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,`vote_for` int(10), `vote_count` int(10));';
        $wpdb->query( $creation_query );
    }

    public function vote($product){
        global $wpdb;
        $like=true;
        if($product<0){
            $product=(-1)*$product;
            $like=false;
        }
        $vote_query = "SELECT vote_count FROM ".$wpdb->get_blog_prefix()."voting WHERE vote_for = $product";
        $result = $wpdb->get_var( $vote_query );
        if(is_null($result)){
            $add_query = "INSERT INTO ".$wpdb->get_blog_prefix()."voting SET vote_for = $product, vote_count =0";
            $wpdb->query( $add_query );
        }

        $voted = explode(';',$_COOKIE['simple-voting']);
        if ($like){ // это было ДОБАВЛЕНИЕ голоса
            $voted[] = $product;
            $result++;

        } else{
            $voted=array_diff($voted,[$product]);
            $result--;
        }
        $vote_query = "UPDATE ".$wpdb->get_blog_prefix()."voting  SET vote_count = $result WHERE vote_for = $product";
        $wpdb->query( $vote_query );
        setcookie('simple-voting',implode(';',$voted),time()+60*60*24*30, '/');

    }
    public function is_voted($product){
        $voted = explode(';',$_COOKIE['simple-voting']);
        if (in_array($product, $voted)){
            return true;
        }
        return false;
    }



    public static function get_instance(){
        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Simple_voting ) ) {
            self::$instance = new Simple_voting();
        }

        return self::$instance;
    }
}

register_activation_hook( __FILE__, 's_voting_activate' );
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
add_action('admin_menu', 'simple_voting_admin_menu' );
add_filter('votebutton' , 'vote_button');

if( wp_doing_ajax() ){
    add_action('wp_ajax_simple_vote', 'simple_voting_ajax_handler');
    add_action('wp_ajax_nopriv_simple_vote', 'simple_voting_ajax_handler');
}

add_shortcode( 'votebutton', 'vote_shortcode_function' );

function vote_shortcode_function( $atts ){

    return vote_button($atts['id']);
}


function simple_voting_ajax_handler(){
    $namevote = $_POST['votefor'];
    Simple_voting::vote($namevote);
    echo 'Voted';
    wp_die();
}

function my_scripts_method(){
    $mjs='/wp-content/plugins/simple-voting/';
    wp_enqueue_script( 'simple-voting-js', $mjs.'js/simple-voting.js');
    wp_enqueue_style( 'simple-voting-css', $mjs.'css/simple-voting.css' );

}
function vote_button( $productId){
    if (!Simple_voting::is_voted($productId)) {
        return "<a  class='simple-voting-vote-button'  href='#' votedata='{$productId}'>Like</a>";
    }
    return "<a  class='simple-voting-unvote-button'  href='#' votedata='-{$productId}'>Liked</a>";
}


function simple_voting_admin_menu() {
    add_menu_page('Голосовалка в админке', 'Голосовалка', 'manage_options', 'simple-voting-admin', 'simple_voting_report', 'dashicons-edit' );
}

function simple_voting_report(){
    global $wpdb;
    echo 'Голосовалка <br>';
    echo 'шорткод вида [votebutton id=14]  где 14 - product_id <br>';
    echo 'вставка в php  вида echo vote_button(14)  где 14 - product_id <br>';

    echo '<h1>Результаты голосования</h1>';
    $query =  "SELECT `vote_for` AS PRODUCT , vote_count AS VOTED FROM `".$wpdb->get_blog_prefix()."voting`  WHERE vote_count>0 ORDER BY VOTED desc ";
    $result = $wpdb->get_results( $query );
    $output = "<table><tr><th>Product ID</th><th>VOTED</th></tr>";
    if($result){
        foreach ($result as $res){
            $output .= "<tr><td>{$res->PRODUCT}</td><td>{$res->VOTED}</td></tr>";
        }
    }
    $output .="</table>";
    echo $output;
}
function s_voting_activate(){
        Simple_voting::createTable();
}
//$simple_voting = Simple_voting::get_instance();